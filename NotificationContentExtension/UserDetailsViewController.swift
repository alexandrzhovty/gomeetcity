//
//  UserDetailsViewController.swift
//  NotificationContentExtension
//
//  Created by Sergey Krotkih on 1/9/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class UserDetailsViewController: UIViewController, UNNotificationContentExtension {
   
   @IBOutlet var label: UILabel?
   @IBOutlet weak var webView: UIWebView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      let filePath = Bundle.main.path(forResource: "matrix", ofType: "gif")
      let gif = NSData(contentsOfFile: filePath!)!
      webView.load(gif as Data, mimeType: "image/gif", textEncodingName: String(), baseURL: URL(string: filePath!)!)
      webView.isUserInteractionEnabled = false;
   }
   
   @available(iOSApplicationExtension 10.0, *)
   func didReceive(_ notification: UNNotification) {
      self.label?.text = notification.request.content.body
   }
   
}
