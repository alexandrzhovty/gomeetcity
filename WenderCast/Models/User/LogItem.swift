//
//  FilterItem.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/11/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase

class FilterItem: NSObject {
   var ref: FIRDatabaseReference?
   var key: String
   var userId: String
   var gender: String
   var age: String
   var intention: String
   var height: String
   var bodytype: String
   var ethnicity: String
   
   override init() {
      key = ""
      userId = ""
      gender = ""
      age = ""
      intention = ""
      height = ""
      bodytype = ""
      ethnicity = ""
   }
   
   convenience init(filter: FilterModel) {
      self.init()
      self.key = filter.key
      self.userId = filter.userId
      self.ref = filter.ref as? FIRDatabaseReference
      self.age = self.arrayToString(filter.age)
      self.height = self.arrayToString(filter.height)
      self.gender = self.toString(filter.gender)
      self.intention = self.toString(filter.intention)
      self.bodytype = self.toString(filter.bodytype)
      self.ethnicity = self.toString(filter.ethnicity)

   }
   
   init(snapshot: FIRDataSnapshot) {
      key = snapshot.key
      let snapshotValue = snapshot.value as! [String: AnyObject]
      userId = snapshotValue[FilterFields.userId] as! String
      gender = snapshotValue[FilterFields.gender] as! String
      age = snapshotValue[FilterFields.age] as! String
      intention = snapshotValue[FilterFields.intention] as! String
      height = snapshotValue[FilterFields.height] as! String
      bodytype = snapshotValue[FilterFields.bodytype] as! String
      ethnicity = snapshotValue[FilterFields.ethnicity] as! String
      ref = snapshot.ref
   }
   
   func toDictionary() -> [String : Any] {
      return [
         UserFields.userId: userId,
         FilterFields.gender: gender,
         FilterFields.age: age,
         FilterFields.intention: intention,
         FilterFields.height: height,
         FilterFields.bodytype: bodytype,
         FilterFields.ethnicity: ethnicity
      ]
   }

   func update(completion: @escaping (Bool) -> Void) {
      // [START write_fan_out]
      let childUpdates = ["/\(FilterFields.child)/\(self.userId)": self.toDictionary()]
      RemoteServer.sharedInstance.firebaseRef.updateChildValues(childUpdates, withCompletionBlock: { (error, ref) in
         completion(true)
      })
      // [END write_fan_out]
   }

   func remove() {
      self.ref?.removeValue()
   }
}

// MARK: - Private

extension FilterItem {

   func arrayToString(_ filters: [String]?) -> String {
      if let filters = filters {
         let string = filters.joined(separator: ",")
         return string
      } else {
         return ""
      }
   }
   
   func toString(_ filters: [Int]?) -> String {
      if let filters = filters {
         let strings = filters.map{ String($0)}
         let string = strings.joined(separator: ",")
         return string
      } else {
         return ""
      }
   }
}
