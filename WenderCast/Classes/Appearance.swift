//
//  Appearance.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class Appearance {
    
    //    MARK: - Class functions
    class func customize() {
        
        
        // Color file template should be added into the project.
        UIApplication.shared.keyWindow?.tintColor  = UIColor.tint
        
        Appearance.customizeNavigationBar()
    }
    
    
    class func customizeNavigationBar() {
        let navBar = UINavigationBar.appearance()
        navBar.tintColor = UIColor.navigation.tintColor
        
        // Navigation bar title
        let attr: [String : Any] = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont.title.navigationBar
        ]
        navBar.titleTextAttributes = attr
        
        // Background color
        navBar.isTranslucent = false
        navBar.setBackgroundImage( UIImage(Background.navigationBar), for: .default)
//        navBar.shadowImage = UIImage(asset: .navigationShadow)
        
    }
    
    class func customize(viewController: UIViewController) {
        if viewController.parent is UINavigationController {
            // Hides text in back barbutton item
            viewController.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}
