//
//  Strings.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

struct Strings {
    private init() {}
}

extension Strings {
    static let OKString = NSLocalizedString("OK", comment: "OK button")
    static let CancelString = NSLocalizedString("Cancel", comment: "Label for Cancel button")
}

// MARK: - Login controller
extension Strings {
    public static let LogInWithFacebook = NSLocalizedString("Log in with Facebook", comment: "Start controller")
    public static let WeDontPostAnything = NSLocalizedString("We don`t post anything on Facebook.\nBy signing in, you agree to our Privacy Policy", comment: "Start controller")
    public static let PrivacyPolicy = NSLocalizedString("Privacy Policy", comment: "")
    
    public static let InternalErrorTryLater = NSLocalizedString("Internal error. Please repeat later", comment: "")
    
    public static let RegistrationFailed = NSLocalizedString("Registration is failed", comment: "")
    
    public static let PleaseTryLater = NSLocalizedString("Please try it again later", comment: "")
}

// MARK: - Tour list data provider
extension Strings {
    public static let Tour0Text = NSLocalizedString("FIND NEW PEOPLE\nNEARBY WHO WANT\nTO MEET NOW", comment: "Tour")
    public static let Tour1Text = NSLocalizedString("Find Someone Nearby\nand Meet Them Today", comment: "Tour")
    public static let Tour2Text = NSLocalizedString("Send an Invite and Meet Now,\nin real time", comment: "Tour")
    public static let Tour3Text = NSLocalizedString("View Profiles and Rate Your\nMeeting Experiences", comment: "Tour")
    public static let Tour4Text = NSLocalizedString("Easily Navigate Map\nfor Your Meeting", comment: "Tour")
}


// MARK: - Browser
extension Strings {
    public static let UnableToOpenURLErrorTitle = NSLocalizedString("Browser", comment: "")
    public static let UnableToOpenURLError = NSLocalizedString("Unable to open URL", comment: "")
}


extension Strings {
    public static let stateExpired = NSLocalizedString("Expired", comment: "")
    public static let stateReviewing = NSLocalizedString("Reviewing", comment: "")
    public static let stateChecked = NSLocalizedString("Checked", comment: "")
    public static let stateClosed = NSLocalizedString("Closed", comment: "")
    public static let stateAccepted = NSLocalizedString("Accepted", comment: "")
    public static let stateRefused = NSLocalizedString("Refused", comment: "")
    public static let stateJoined = NSLocalizedString("Joined", comment: "")
}

extension Strings {
    public static let cannotLodPhotoFromFacebook = NSLocalizedString("Can't download photo from Facebook", comment: "")
}
