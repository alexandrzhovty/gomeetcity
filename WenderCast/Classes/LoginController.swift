//
//  LoginController.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

import Firebase
import FBSDKLoginKit


//    MARK: -
extension LoginController: SegueHandler {
    //    MARK: SegueHandler protocol
    internal enum SegueType: String {
        case privacyPolicy
        case showTourList
    }
}

class LoginController: UIViewController {

    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var privacyLabel: UILabel!
    
    @IBOutlet weak var facebookView: UIView!
    
    fileprivate var model: UsersPresenterModel = UsersManager
    fileprivate var currentUser: UserModel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookButton.setTitle(Strings.LogInWithFacebook.localizedUppercase, for: .normal)
        privacyLabel.text = Strings.WeDontPostAnything
        facebookView.layer.addBorder(edge: .top, color: UIColor.separator.loginController, thickness: 1)
    
        let link = [NSForegroundColorAttributeName: UIColor.clickable.text]
        
        let attrString = NSMutableAttributedString(attributedString: privacyLabel.attributedText!)
        attrString.singleOut(Strings.PrivacyPolicy, with: link)
        privacyLabel.attributedText = attrString
        
    
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view.addGestureRecognizer(tapGesture)
        
        
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                print("\(user) is signed in")
            } else {
                print("no user signed in")
            }
        }
        
    }
   

    //    MARK: - Outlets
    //    MARK: Gesture recognizers
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
        
        let occuarance = Strings.PrivacyPolicy
        let label = privacyLabel!
        
        guard
            let foundRange = label.attributedText?.string.range(of: occuarance),
            let range = label.attributedText?.string.nsRange(from: foundRange),
            let rect = label.boundingRectForCharacterRange(range: range)
            else {
                return
        }
        
        
        // Increase rect
        if rect.insetBy(dx: -10, dy: -15).contains(gesture.location(in: label)) == true {
            let vc = UIStoryboard(.utility).instantiateViewController(BrowserController.self)
            vc.url = Resources.PDF.privacyPolicy.url
            vc.title = Strings.PrivacyPolicy
            let navVC = UINavigationController(rootViewController: vc)
            present(navVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func didTapFacebook(_ button: Any) {
        
        
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                print("Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            // Perform login by calling Firebase APIs
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    print("Login error: \(error.localizedDescription)")
                    let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                    let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(okayAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                // Present the main view
                if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MainView") {
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
            
        }
        
    
//      FacebookLogin.login(viewController: self, completion: { (fbid, name, email, photoUrl, birthday, gender) in
//        
//        print("we are here")
//        
//         self.model.alreadyRegistreredUser(name: name, email: email) { user in
//            if let user = user {
//               UsersManager.currentUser = user
//               let appDelegate = UIApplication.shared.delegate as! AppDelegate
//               appDelegate.presentMainScreen()
//            } else {
////               self.activityIndicatorView.isHidden = false
//               self.model.signUp(name: name, email: email, password: "", fbphotoUrl: photoUrl, birthday: birthday, gender: gender) { user in
////                  self.activityIndicatorView.isHidden = true
//                  if let user = user {
//                     DispatchQueue.main.async {
//                        self.currentUser = user
////                        self.performSegue(withIdentifier: Segue.Registration, sender: self)
//                     }
//                  } else {
//                     Alert.default.showOk(Strings.RegistrationFailed, message: Strings.PleaseTryLater)
//                  }
//               }
//            }
//         }
//      }, failed: {
//         Alert.default.showOk("", message: Strings.InternalErrorTryLater)
//      })
   }
}


