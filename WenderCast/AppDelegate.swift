//
//  AppDelegate.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/11/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	
	static var shared: AppDelegate {
		guard let `self` = UIApplication.shared.delegate as? AppDelegate else {
			fatalError()
		}
		return self
	}
	
	var rootVC: UINavigationController {
		guard let navVC = self.window?.rootViewController as? UINavigationController else {
			fatalError()
		}
		return navVC
	}
	
	var generalUser: MeetUser?
	
	// MARK: - State application changing
	
	func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
		return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		Appearance.customize()
		
//		self.putLaunchScreensot()
		
		Fabric.with([Crashlytics.self])
			
		FirebaseApp.configure()
		
		GMSServices.provideAPIKey(GoogleMaps.GoogleMapsAPIKey)
		
		
		FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
		
		
		// It doesn't work with old version
		if UserDefaults.standard.bool(forKey: "SecondLogin") == false {
			presentLoginScreen()
			return true
		}
		
		if let currentUser = Auth.auth().currentUser {
			let userRef = FirebaseStack.users.child( currentUser.uid)
			userRef.observe(.value, with: { snapshot in
				
				
				if snapshot.exists() {
					print("exists")
					userRef.removeAllObservers()
					self.generalUser = MeetUser(snapshot: snapshot)
					
					self.presentMainScreen()
					
				} else {
					var dict = [String: Any]()
					dict["name"] = currentUser.displayName?.components(separatedBy: " ").first ?? ""
					dict["email"] = currentUser.email
					dict["joined"] = Date().timeIntervalSince1970
					userRef.setValue(dict)
					
				}
				
			})

			
		} else {
			presentLoginScreen()
			
		}

		return true
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		FBSDKAppEvents.activateApp()

	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		FBSDKAppEvents.activateApp()
	}
	
}

// MARK: - Private

extension AppDelegate {
	func presentLoginScreen() {
		
		
		    initRootViewControllerWithStoryboard(.login)
////		initRootViewControllerWithStoryboard(.main)
//		initRootViewControllerWithStoryboard(.editor)
	}
	
	func presentMainScreen() {
		let vc = UIStoryboard(.profile).instantiateViewController(UserProfileController.self)
		let vc2 = UIStoryboard(.dashboard).instantiateViewController(NearbyListController.self)
		rootVC.setNavigationBarHidden(false, animated: true)
		rootVC.setViewControllers([vc, vc2], animated: false)
//		rootVC.setViewControllers([vc], animated: false)
	}
	
	fileprivate func initRootViewControllerWithStoryboard(_ type: StroyboadType) {
		guard let window = self.window else {
			assertionFailure()
			return
		}
		window.rootViewController = UIStoryboard(type).instantiateInitialViewController()
	}
}

//extension AppDelegate {
//	
//	func stopFakeSplashScreen() {
////		if let startScreenImage = self.startScreenImage {
////			startScreenImage.removeFromSuperview()
////			self.startScreenImage = nil
//		}
//	}
//	
//	fileprivate func putLaunchScreensot() {
////		let screenshotWindow = screenshotWithContentView(self.window!)
////		let imageView  = UIImageView(frame: (self.window?.bounds)!)
////		imageView.image = screenshotWindow
////		self.window?.rootViewController?.view.addSubview(imageView)
////		self.startScreenImage = imageView
//	}
//	
//}


