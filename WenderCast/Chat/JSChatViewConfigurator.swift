//
//  JSChatViewConfigurator.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 2/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

class JSChatViewConfigurator {
   
   class func configure(_ viewController: JSChatViewController) {
      let mediaInteractor = JSChatMediaInteractor()
      viewController.mediaInteractor = mediaInteractor
      mediaInteractor.viewController = viewController
      mediaInteractor.output = viewController
      
   }
}
