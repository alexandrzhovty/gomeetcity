//
//  GlobalColors.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/9/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
/**
class GlobalColors {
   static let kTabBarColor = UIColor(hexString: "FF3366")
   
   static let kSelectedTabBarColor = UIColor(hexString: "7B1A1A")
   
   static let kImagesBackgroundColor = UIColor.white
   
   static let kFontLightGrayColor = UIColor(hexString: "838383")
   
   static let kDarkGreenColor = UIColor(hexString: "1E6869")
   static let kLeightGreenColor = UIColor(hexString: "45A177")
   
   static let kTableTitleTextColor = UIColor(hexString: "1E1E27")
   
   static let kChatBackgroundCell = UIColor(hexString: "EFEAEA")
   
   static let kGrayButtonBackgroundColor = UIColor(hexString: "7A7A7A")
   static let kOrangeButtonBackgroundColor = UIColor(hexString: "FF7132")
   static let kBlueTextColor = UIColor(hexString: "4AACF3")

   static let kGreenAcceptColor = UIColor(hexString: "007E16")
   
   static let kLoginNavBarColor = UIColor(hexString: "F9F9F9")
   
   static let kFacebookColor = UIColor(hexString: "4267B2")
   
   static let kTextFieldTextColorBlueGrey = UIColor(red: 0.6156862745098, green: 0.62745098039216, blue: 0.69019607843137, alpha: 1)
   
   func getRandomColor() -> UIColor{
      let randomRed:CGFloat = CGFloat(drand48())
      let randomGreen:CGFloat = CGFloat(drand48())
      let randomBlue:CGFloat = CGFloat(drand48())
      return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
   }
}
*/
