//
//  JSChatTestData.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 4/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import JSQMessagesViewController


class JSChatTestData: JSQMessagesViewController {

   var messages = [JSQMessage]()   
   
   func receiveMessagePressed(_ sender: UIBarButtonItem) {
      /**
       *  DEMO ONLY
       *
       *  The following is simply to simulate received messages for the demo.
       *  Do not actually do this.
       */
      
      /**
       *  Show the typing indicator to be shown
       */
      self.showTypingIndicator = !self.showTypingIndicator
      
      /**
       *  Scroll to actually view the indicator
       */
      self.scrollToBottom(animated: true)
      
      /**
       *  Copy last sent message, this will be the new "received" message
       */
      var copyMessage = self.messages.last?.copy()
      
      if (copyMessage == nil) {
         copyMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: "First received!")
      }
      
      var newMessage: JSQMessage!
      var newMediaData: JSQMessageMediaData!
      var newMediaAttachmentCopy: AnyObject?
      
      if (copyMessage! as AnyObject).isMediaMessage() {
         /**
          *  Last message was a media message
          */
         let copyMediaData = (copyMessage! as AnyObject).media
         
         switch copyMediaData {
         case is JSQPhotoMediaItem:
            let photoItemCopy = (copyMediaData as! JSQPhotoMediaItem).copy() as! JSQPhotoMediaItem
            photoItemCopy.appliesMediaViewMaskAsOutgoing = false
            
            newMediaAttachmentCopy = UIImage(cgImage: photoItemCopy.image!.cgImage!)
            
            /**
             *  Set image to nil to simulate "downloading" the image
             *  and show the placeholder view5017
             */
            photoItemCopy.image = nil;
            
            newMediaData = photoItemCopy
         case is JSQLocationMediaItem:
            let locationItemCopy = (copyMediaData as! JSQLocationMediaItem).copy() as! JSQLocationMediaItem
            locationItemCopy.appliesMediaViewMaskAsOutgoing = false
            newMediaAttachmentCopy = locationItemCopy.location!.copy() as AnyObject?
            
            /**
             *  Set location to nil to simulate "downloading" the location data
             */
            locationItemCopy.location = nil;
            
            newMediaData = locationItemCopy;
         case is JSQVideoMediaItem:
            let videoItemCopy = (copyMediaData as! JSQVideoMediaItem).copy() as! JSQVideoMediaItem
            videoItemCopy.appliesMediaViewMaskAsOutgoing = false
            newMediaAttachmentCopy = (videoItemCopy.fileURL! as NSURL).copy() as AnyObject?
            
            /**
             *  Reset video item to simulate "downloading" the video
             */
            videoItemCopy.fileURL = nil;
            videoItemCopy.isReadyToPlay = false;
            
            newMediaData = videoItemCopy;
         case is JSQAudioMediaItem:
            let audioItemCopy = (copyMediaData as! JSQAudioMediaItem).copy() as! JSQAudioMediaItem
            audioItemCopy.appliesMediaViewMaskAsOutgoing = false
            newMediaAttachmentCopy = (audioItemCopy.audioData! as NSData).copy() as AnyObject?
            
            /**
             *  Reset audio item to simulate "downloading" the audio
             */
            audioItemCopy.audioData = nil;
            
            newMediaData = audioItemCopy;
         default:
            assertionFailure("Error: This Media type was not recognised")
         }
         newMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, media: newMediaData)
      }
      else {
         /**
          *  Last message was a text message
          */
         newMessage = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: (copyMessage! as AnyObject).text)
      }
      
      /**
       *  Upon receiving a message, you should:
       *
       *  1. Play sound (optional)
       *  2. Add new JSQMessageData object to your data source
       *  3. Call `finishReceivingMessage`
       */
      
      self.messages.append(newMessage)
      self.finishReceivingMessage(animated: true)
      
      if newMessage.isMediaMessage {
         /**
          *  Simulate "downloading" media
          */
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            /**
             *  Media is "finished downloading", re-display visible cells
             *
             *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
             *
             *  Reload the specific item, or simply call `reloadData`
             */
            
            switch newMediaData {
            case is JSQPhotoMediaItem:
               (newMediaData as! JSQPhotoMediaItem).image = newMediaAttachmentCopy as? UIImage
               self.collectionView!.reloadData()
            case is JSQLocationMediaItem:
               (newMediaData as! JSQLocationMediaItem).setLocation(newMediaAttachmentCopy as? CLLocation, withCompletionHandler: {
                  self.collectionView!.reloadData()
               })
            case is JSQVideoMediaItem:
               (newMediaData as! JSQVideoMediaItem).fileURL = newMediaAttachmentCopy as? URL
               (newMediaData as! JSQVideoMediaItem).isReadyToPlay = true
               self.collectionView!.reloadData()
            case is JSQAudioMediaItem:
               (newMediaData as! JSQAudioMediaItem).audioData = newMediaAttachmentCopy as? Data
               self.collectionView!.reloadData()
            default:
               assertionFailure("Error: This Media type was not recognised")
            }
         }
      }
   }
   
}
