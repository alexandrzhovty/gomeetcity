//
//  NearbyListController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

struct MockUser {
	let name: String
	let age: Int
	let distanse: Double
	let image: UIImage
}


protocol NearbyListControllerProtocol: class {
	func setNearbyListHidden(_ hidden: Bool, animated: Bool)
}

//    MARK: - Properties & variables
final class NearbyListController: UIViewController {
    //    MARK: Public
    
    //    MARK: Outlets
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var mapContainer: UIView!
	@IBOutlet weak var mapHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var mapTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var collectionTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var logoImageView: UIImageView!
	@IBOutlet weak var logoVerticalConstraint: NSLayoutConstraint!
	@IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
	
    //    MARK: Private
	fileprivate var _mapController: NearbyMapController!
	fileprivate var _headerView: UIView!
	fileprivate var _dataSource: [MockUser]!
	fileprivate var _mapContainerHeight: CGFloat = 0
	fileprivate let _distanceFormatter = MKDistanceFormatter()
	fileprivate var _logoImageViewWidth: CGFloat = 0
    
    //    MARK: - Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	private func commonInit() {
		_headerView = HeaderView(frame: CGRect.zero)
	}
	
}

//    MARK: - View life cycle
extension NearbyListController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
		navigationItem.title = Strings.PeopleClosestToYou

        
        // Customize appearance
        Appearance.customize(viewController: self)
		self.navigationItem.setHidesBackButton(true, animated: false)
		
		
		// Header view
		configure(_headerView)
		configure(collectionView)
		
		_mapContainerHeight = mapHeightConstraint.constant
		
        _logoImageViewWidth = logoImageView.frame.width  //logoWidthConstraint.constant
		print(String(describing: type(of: self)),":", #function)
		print("_logoImageViewWidth ", _logoImageViewWidth)
        /*
         // Configure tableView
         configure(tableView)
         */
		
		_dataSource = [
			MockUser(name: "Rosaline, 33", age: 33, distanse: 700, image: UIImage(named: "Face-1")!),
			MockUser(name: "Madonna, 21", age: 21, distanse: 800, image: UIImage(named: "Face-2")!),
			
			MockUser(name: "Jessica, 18", age: 33, distanse: 2000, image: UIImage(named: "Face-3")!),
			MockUser(name: "Cecelia, 41", age: 33, distanse: 4000, image: UIImage(named: "Face-4")!),
			
			MockUser(name: "Anna, 24", age: 33, distanse: 5000, image: UIImage(named: "Face-5")!),
			MockUser(name: "Amanda, 38", age: 33, distanse: 5500, image: UIImage(named: "Face-6")!),
			
			MockUser(name: "Angelina, 26", age: 33, distanse: 6000, image: UIImage(named: "Face-7")!),
			MockUser(name: "Holly, 29", age: 33, distanse: 7000, image: UIImage(named: "Face-8")!),
			
			MockUser(name: "Amanda, 19", age: 33, distanse: 5500, image: UIImage(named: "Face-9")!),
			MockUser(name: "Amanda, 42", age: 33, distanse: 5500, image: UIImage(named: "Face-10")!),
			
		]
    }
}

//    MARK: - Utilities
extension NearbyListController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case _headerView:
			var frame = CGRect.zero
			frame.size.width = self.view.bounds.width
			frame.size.height = self.mapHeightConstraint.constant
			frame.origin.y = frame.height * (-1.0)
			
			_headerView.frame = frame
			_headerView.autoresizingMask = [ .flexibleWidth ]
			
			let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
			_headerView.addGestureRecognizer(tap)
			
		case collectionView:
			collectionView.contentInset = UIEdgeInsets(top: _headerView.frame.height, left: 0, bottom: 0, right: 0)
			collectionView.addSubview(_headerView)
			collectionView.backgroundColor = UIColor.clear
			self.view.backgroundColor = UIColor(.pink)
			
        default: break
        }
        
    }
}

//    MARK: - Outlet functions
extension NearbyListController  {
    //    MARK: Buttons
	@IBAction func didTapProfile(_ button: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
    //    MARK: Gesture handlers
	@objc func handleTapGesture(_ recognizer: UITapGestureRecognizer) {
		setNearbyListHidden(true, animated: true)
	}
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyListController: SegueHandler {
    enum SegueType: String {
        case showFilter
		case embedMap
		case showNearByProfile
    }
	
	@IBAction func exitToNearbyListController(_ segue: UIStoryboardSegue) {
//		print(String(describing: type(of: self)),":", #function)
	}
    
	
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .showNearByProfile:
			guard let indexPath = collectionView.indexPathsForSelectedItems?.first else {
				break
			}
			let vc = segue.destination as! NearbyProfileController
			vc.mockUser = _dataSource[indexPath.row]
			
			
		case .embedMap:
			_mapController = segue.destination as! NearbyMapController
			_mapController.parentController = self
			
        case .showFilter: break
			
		}
    }
}


// MARK: - Neearby list protocol
extension NearbyListController: NearbyListControllerProtocol {
	func setNearbyListHidden(_ hidden: Bool, animated: Bool) {
		if hidden {
			_mapController.peopleButton.alpha = 0
			_mapController.peopleButton.isHidden = false
			
			collectionTopConstraint.constant = self.view.bounds.height
			mapHeightConstraint.constant = collectionView.bounds.height
			
			UIView.animate(withDuration: 0.25) {
				[unowned self] in
				self._mapController.peopleButton.alpha = 1
				
				self.view.layoutIfNeeded()
			}
		} else {
			
			collectionTopConstraint.constant = 0
			mapHeightConstraint.constant = _mapContainerHeight
			
			UIView.animate(withDuration: 0.25, animations: {
				[unowned self] in
				self._mapController.peopleButton.alpha = 0
				
				self.view.layoutIfNeeded()
			}, completion: { finished in
				self._mapController.peopleButton.isHidden = true
				
				if let coordinate = self._mapController.mapView.myLocation?.coordinate {
					let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
					self._mapController.mapView.animate(to: camera)
				}
			})
		}
	}
}


// MARK: - Collection view protocol
extension NearbyListController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	//    MARK: Datatsourse
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return _dataSource.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(NearbyCollectionCell.self, for: indexPath) as! NearbyCollectionCell
		let user = _dataSource[indexPath.row]
		cell.titleLabel.text = user.name
		cell.imageView.image = user.image
		cell.distanceLabel.text = _distanceFormatter.string(fromDistance: user.distanse)
		return cell
	}
	
	//    MARK: Deleage flow layout
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let width = collectionView.bounds.width / 2.0
		let height = width / 375 * 262
		return CGSize(width: width, height: height)
	}
}

// MARK: - Scroll view protocol
extension NearbyListController:UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let offset = scrollView.contentOffset
		let delta = mapHeightConstraint.constant + offset.y
		if delta > 0 && delta < mapHeightConstraint.constant {
			
			mapTopConstraint.constant = delta / 2 * (-1)
			let alpha = mapHeightConstraint.constant / 200 * delta / 600
			_headerView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
			
			logoWidthConstraint.constant = _logoImageViewWidth
			
		}
		else if delta <= 0 {
			_headerView.backgroundColor = UIColor.black.withAlphaComponent(0)
			let decreaser = (mapTopConstraint.constant - delta) / 10
			let newValue = _logoImageViewWidth + decreaser
			logoWidthConstraint.constant = newValue
			mapTopConstraint.constant = delta * (-1)
			logoVerticalConstraint.constant = mapTopConstraint.constant / 2
			
		}
		
	}
}

