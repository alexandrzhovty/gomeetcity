//
//  ProfileManagerController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */


//    MARK: - Observers protocol
@objc protocol ObserverProtocol: class {
	var observers: [Any] { get set }
	func registerObserver()
	func unregisterObserver()
}


//    MARK: - Properties & variables
extension ProfileManagerController: ProfilePresenterControllerProtocol {}

final class ProfileManagerController: UIViewController {
    //    MARK: Public
	var userModel: MeetUserViewModel!
	var presenter: ProfilePresenter!
	var dataSource: DataSource!
	let imagePicker = UIImagePickerController()
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	
    //    MARK: Private
	fileprivate var _observers = [Any]()
	fileprivate var _photoSelectionView: PhotoSelectionView!
	
    //    MARK: Enums & Structures
	
	//    MARK: Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commotInt()
	}
	
	private func commotInt() {
		
	}
	
	deinit {
		unregisterObserver()
	}
	
}

//    MARK: - View life cycle
extension ProfileManagerController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEVELOPMENT
            navigationItem.title = NSLocalizedString("Development", comment: "").localizedUppercase
        #endif
        
        // Customize appearance
        Appearance.customize(viewController: self)
		
		
		dataSource = ProfileManagerDataSource(with: self)
		presenter = ProfilePresenter(with: self)
		
		// Configure tableView
		configure(tableView)
		presenter.setup(tableView)
		
		
		
		
//		tableView.registerHeaderFooter()
//		tableView.registerHeaderFooter(HeaderSectionView.self)
		
		
		
		registerObserver()
		
    }
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		
		if self._photoSelectionView == nil {
			_photoSelectionView = PhotoSelectionView.instantiateFromInterfaceBuilder()
			
			let view = UIView()
			let size = CGSize(width: self.view.bounds.width, height: 2000.0)
			view.backgroundColor = UIColor.clear

			view.frame.size = PhotoSelectionView.sizeThatFits(size)
			
			_photoSelectionView = PhotoSelectionView.instantiateFromInterfaceBuilder()
			_photoSelectionView.frame = view.bounds
			_photoSelectionView.autoresizingMask = [ .flexibleWidth, .flexibleHeight ]
			view.addSubview(_photoSelectionView)
			tableView.tableHeaderView = view
			
			_photoSelectionView.managerController = self
			_photoSelectionView.userModel = self.userModel
		}
	}
}

//    MARK: - Utilities
extension ProfileManagerController: ObserverProtocol {
	var observers: [Any] {
		get { return _observers }
		set { _observers = newValue }
	}
	
	func registerObserver() {
		userModel.user.addObserver(self, forKeyPath: #keyPath(MeetUser.aboutme), options: .new, context: nil)
		userModel.user.addObserver(self, forKeyPath: #keyPath(MeetUser.occupation), options: .new, context: nil)
//		userModel.user.addObserver(self, forKeyPath: #keyPath(Gender), options: .new, context: nil)
	}
	
	func unregisterObserver() {
		userModel.user.removeObserver(self, forKeyPath: #keyPath(MeetUser.aboutme))
		userModel.user.removeObserver(self, forKeyPath: #keyPath(MeetUser.occupation))
//		userModel.user.removeObserver(self, forKeyPath: #keyPath(Gender))
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		switch keyPath ?? "" {
		case #keyPath(MeetUser.aboutme), #keyPath(MeetUser.occupation):
			
			// Update row height for dynamic TextView
			let currentOffset = tableView.contentOffset
			UIView.setAnimationsEnabled(false)
			tableView.beginUpdates()
			tableView.endUpdates()
			UIView.setAnimationsEnabled(true)
			tableView.setContentOffset(currentOffset, animated: false)
			
		default:
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
}

//    MARK: - Utilities
extension ProfileManagerController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case tableView:
			tableView.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.estimatedRowHeight = Constants.TableView.profileCellHeight
			tableView.rowHeight = UITableViewAutomaticDimension
			
        default: break
        }
		
    }
}

//    MARK: - Outlet functions
extension ProfileManagerController  {
    //    MARK: Buttons
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension ProfileManagerController: SegueHandler {
    enum SegueType: String {
        case exitFromProfileManager
    }
	
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .exitFromProfileManager: break		
        }
    }

}

//	MARK: - Table view protocol
extension ProfileManagerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		print(String(describing: type(of: self)),":", #function)
		if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
			
			let minSize: CGFloat = 600
			let size = image.size
			let ratio: CGFloat
			if size.width > size.height {
				ratio = size.width / minSize
			} else {
				ratio = size.height / minSize
			}
			let newSize = CGSize(width: size.width / ratio, height: size.height / ratio)
			
			let cgImage = image.cgImage!
			
			let bitsPerComponent = cgImage.bitsPerComponent
			let bytesPerRow = cgImage.bytesPerRow
			let colorSpace = cgImage.colorSpace
			let bitmapInfo = cgImage.bitmapInfo
			
			
			let context = CGContext(data: nil, width: Int(newSize.width), height: Int(newSize.height), bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: bitmapInfo.rawValue)
			context?.interpolationQuality = .high
			context?.draw(cgImage, in: CGRect(x: 0.0,y: 0.0, width: newSize.width, height: newSize.height))

			
//			var scaledImage = UIImage(cgImage: context!.makeImage()!)
			
			if let cgi = context!.makeImage() {
				_photoSelectionView.add(UIImage(cgImage: cgi))
			}
			
			
		}
		
		dismiss(animated: true, completion: nil)
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}
}
