//
//  CountryCodePresentationController.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 13.12.16.
//  Copyright © 2016 Lindenvalley GmbH. All rights reserved.
//

import UIKit

class CountryCodePresentation: UIPresentationController {

    lazy var dimmingView :UIView = { [unowned self] in
        let view = UIView(frame: self.containerView!.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.alpha = 0.0
        return view
    }()
    
    override func presentationTransitionWillBegin() {
        
        guard
            let containerView = containerView,
            let presentedView = presentedView
            else {
                return
        }
        
        // Add the dimming view and the presented view to the heirarchy
        dimmingView.frame = containerView.bounds
        containerView.addSubview(dimmingView)
        containerView.addSubview(presentedView)
        
        // Fade in the dimming view alongside the transition
        if let transitionCoordinator = self.presentingViewController.transitionCoordinator {
            transitionCoordinator.animate(alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.dimmingView.alpha = 1.0
            }, completion:nil)
        }
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool)  {
        // If the presentation didn't complete, remove the dimming view
        if !completed {
            self.dimmingView.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionWillBegin()  {
        // Fade out the dimming view alongside the transition
        if let transitionCoordinator = self.presentingViewController.transitionCoordinator {
            transitionCoordinator.animate(alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.dimmingView.alpha  = 0.0
            }, completion:nil)
        }
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        // If the dismissal completed, remove the dimming view
        if completed {
            self.dimmingView.removeFromSuperview()
        }
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = containerView else { return CGRect() }
        
        // We don't want the presented view to fill the whole container view, so inset it's frame
        var frame = containerView.bounds;
//        let margin: CGFloat = 50.0
//        frame = frame.insetBy(dx: margin, dy: margin)
		
		frame.size.height = 250
		
        return frame
    }
    
    
    // ---- UIContentContainer protocol methods
//    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator transitionCoordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransitionToSize(size, withTransitionCoordinator: transitionCoordinator)
//        
//        guard
//            let containerView = containerView
//            else {
//                return
//        }
//        
//        transitionCoordinator.animateAlongsideTransition({(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
//            self.dimmingView.frame = containerView.bounds
//        }, completion:nil)
//    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        guard
            let containerView = containerView
            else {
                return
        }
        
        coordinator.animate(alongsideTransition: {(context: UIViewControllerTransitionCoordinatorContext!) -> Void in
            self.dimmingView.frame = containerView.bounds
        }, completion:nil)
        
    }
    
}

//extension CountryCodePresentationController: UIAdaptivePresentationControllerDelegate {
//    //    override func shouldPresentInFullscreen() -> Bool {
//    //        return true
//    //    }
//    //
//    //    override func adaptivePresentationStyle() -> UIModalPresentationStyle {
//    //        return .FullScreen
//    //    }
//    //    
//    override var shouldPresentInFullscreen: Bool { return true }
//    
//    override var adaptivePresentationStyle: UIModalPresentationStyle { return .fullScreen }
//}
