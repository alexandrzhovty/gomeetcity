//
//  GalleryViewModel.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 4/25/17.
//  Copyright © 2017 Lindenvalley GmbH. All rights reserved.
//

import UIKit

struct GalleryViewModel {
    
    var user: MockUser
    var galleryViewType: GalleryViewType
    
    fileprivate var _dataSource = [UIImage]()
    
    init(user: MockUser, galleryViewType: GalleryViewType) {
        self.user = user
        self.galleryViewType = galleryViewType
        
//        guard let id = bar.id else { return }
//        guard let images = bar.images else { return }
//        guard images.count > 0 else { return }
		
//        if let array = images.sortedArray(using: [NSSortDescriptor(key: #keyPath(Image.sortOrder), ascending: false)]) as? [Image] {
//            let ids = array.map{ $0.id }.flatMap{ $0 }
//            self.dataSource = ids.map{ APIManager.request(for: APIMethods.Bar.Image, with: [id, $0]) }.flatMap{ $0 }
//        }
		_dataSource.append(user.image)
		
		
    }
    
    func object(at index: Int) -> UIImage? {
        guard case 0..<_dataSource.count = index else { return nil }
        return _dataSource[index]
    }
    
    func numberOfObjects() -> Int {
        return _dataSource.count
    }
    
}
