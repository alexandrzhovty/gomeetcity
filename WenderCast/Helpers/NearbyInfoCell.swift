//
//  InfoNearbyCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class InfoNearbyCell: UITableViewCell, NibLoadableView {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var statusView: StatusView!
	@IBOutlet weak var ocuppationLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	@IBOutlet weak var aboutMeLabel: UILabel!
	

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
