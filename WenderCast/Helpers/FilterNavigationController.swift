//
//  FilterNavigationController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class FilterNavigationController: UINavigationController {
	
	fileprivate let customTransitionDelegate = TransitioningDelegate()
	
	
	//    MARK: - Initilization
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.commonInit()
	}
	
	override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!)  {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		
		self.commonInit()
	}
	
	func commonInit() {
		modalPresentationStyle = .custom
		transitioningDelegate = self
	}
	
}

extension FilterNavigationController: UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		if presented == self {
			return CountryCodePresentation(presentedViewController: presented, presenting: presenting)
		}
		return nil
	}

}
