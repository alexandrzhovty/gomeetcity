//
//  MeetUser.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON



class MeetUser: NSObject {
	var uid: String
	var name: String?
	var email: String?
	var photos: [MeetPhoto] = [MeetPhoto]()
	dynamic var occupation: String?
	dynamic var aboutme: String = ""
	var gender = Gender.none
	var lookFor = LookFor.none
	var birthday: TimeInterval = 0
	var joined: TimeInterval
	var readytomeet: Bool = false
	var instagramToken: String?
	var instagramId: String?
	
	/// Data and time when User went to offline
	var offline: TimeInterval = 0
	
	/// Show me on Meetcity
	var showMe: Bool = true
	
	/// Don't show my current location
	var locationHidden: Bool = false
	
	var isRegistrationCompleted: Bool {
		return gender != Gender.none && lookFor != LookFor.none && aboutme.isEmpty == true
	}
	
	
	//	MARK: - Initialization

	
	init(uid: String, name: String? = nil, email: String? = nil) {
		self.uid = uid
		self.name = name?.components(separatedBy: " ").first ?? ""
		self.email = email
		self.joined = Date().timeIntervalSince1970
		super.init()
	}
	
	
	convenience init(with firebaseUser: User) {
		self.init(uid: firebaseUser.uid, name: firebaseUser.displayName, email: firebaseUser.email)
	}
	
	init?(snapshot: DataSnapshot) {
		guard let snapshotValue = snapshot.value as? [String: AnyObject] else {
			return nil
		}
		
		let json = JSON(snapshotValue)
		uid = 			json[UserFields.uid].stringValue
		joined =		json[UserFields.joined].double ?? 0

		super.init()
		self.update(with: snapshot)
		
	}
	
	func update(with snapshot: DataSnapshot) {
		guard let snapshotValue = snapshot.value as? [String: AnyObject] else {
			return
		}
		
		let json = JSON(snapshotValue)
		
		uid = 			json[UserFields.uid].stringValue
		name =			json[UserFields.name].string
		email =			json[UserFields.email].string
		aboutme =		json[UserFields.aboutme].stringValue
		gender =		Gender(value: json[UserFields.gender].int)
		lookFor =		LookFor(value: json[UserFields.lookFor].int)  //LookFor[value: json[UserFields.lookFor].int)
		occupation = 	json[UserFields.occupation].string
		
		if birthday <= 0 {
			birthday =		json[UserFields.birthday].double ?? 0
		}
		joined =		json[UserFields.joined].double ?? Date().timeIntervalSinceNow
		readytomeet = 	json[UserFields.readytomeet].bool ?? false
		offline =		json[UserFields.offline].double ?? 0
		
		showMe	=		json[UserFields.showMe].bool ?? true
		locationHidden = json[UserFields.showMe].bool ?? false
		
		instagramToken = json[UserFields.instagramToken].string
		instagramId  = json[UserFields.instagramId].string
		
		
		let array = json[UserFields.photos].array
		for (idx, item) in (array ?? []).enumerated() {
			self.photos.append(MeetPhoto(with: idx, filename: item[PhotosFields.filename].stringValue))
		}
		
		

	}
	
	func toDictionary() -> [String : Any] {
		
		var json = JSON([:])
		
		json[UserFields.uid].string = uid
		json[UserFields.name].string = name
		json[UserFields.email].string = email
		json[UserFields.aboutme].string = aboutme
		json[UserFields.joined].double = joined
		json[UserFields.readytomeet].bool = readytomeet
		json[UserFields.offline].double = offline
		json[UserFields.birthday].double = birthday
		json[UserFields.gender].int = gender.rawValue
		json[UserFields.lookFor].int = lookFor.rawValue
		json[UserFields.occupation].string = occupation
		json[UserFields.showMe].bool = showMe
		json[UserFields.locationHidden].bool = locationHidden
		
		json[UserFields.instagramToken].string = instagramToken
		json[UserFields.instagramId].string = instagramId
		
		
		
//		json[UserFields.photos] =  photos.map{ $0.json }
//		
//		let array = photos.map{ $0.json }
//		print(array)
//		
//		
//		print(json)
		
		return json.rawValue as! [String: Any]
	}
	
	
	
}
