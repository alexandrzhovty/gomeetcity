//
//  TourListDataProvider.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

struct TourListDataProvider {
    fileprivate let items: [Tour]
    
    init() {
        items = [
            Tour(message: Strings.Tour0Text, type: .video(url: Resources.MOV.promo.url)),
            Tour(message: Strings.Tour1Text, type: .image(image: UIImage(Photo.tourIphone))),
            Tour(message: Strings.Tour2Text, type: .image(image: UIImage(Photo.tourIphone2))),
            Tour(message: Strings.Tour3Text, type: .image(image: UIImage(Photo.tourIphone3))),
            Tour(message: Strings.Tour4Text, type: .image(image: UIImage(Photo.tourIphone4)))
        ]
    }    
}

extension TourListDataProvider {
    var numberOfSection: Int { return 1 }
    func numberOfItems(in section: Int) -> Int {
        return items.count
    }
    
    func object(at indexPath: IndexPath) -> Tour {
        return items[indexPath.row]
    }
}
