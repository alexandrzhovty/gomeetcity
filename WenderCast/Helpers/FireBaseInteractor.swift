//
//  FirebaseIterator.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/7/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
https://bitbucket.org/snippets/alexandrzhovty/xnzpA
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseRemoteConfig


class FireBaseInteractor {
    static let `default` = FireBaseInteractor()
    private init() {}
	
	fileprivate var currentFireBaseUser: User?
	fileprivate var remoteConfig: RemoteConfig!
	var displayName: String?
	fileprivate var signInCounter = 0
	
	var firebaseRef: DatabaseReference {
		return Database.database().reference().child(DataBase.name)
	}
	
	func configure() {
		FirebaseApp.configure()
		Database.database().isPersistenceEnabled = true
		configureRemoteConfig()
	}
}



extension FireBaseInteractor {
	
	fileprivate func configureRemoteConfig() {
		remoteConfig = RemoteConfig.remoteConfig()
		// Create Remote Config Setting to enable developer mode.
		// Fetching configs from the server is normally limited to 5 requests per hour.
		// Enabling developer mode allows many more requests to be made per hour, so developers
		// can test different config values during development.
		let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true)
		remoteConfig.configSettings = remoteConfigSettings!
	}
	
	func fetchConfig() {
		var expirationDuration: Double = 3600
		// If in developer mode cacheExpiration is set to 0 so each fetch will retrieve values from
		// the server.
		if (self.remoteConfig.configSettings.isDeveloperModeEnabled) {
			expirationDuration = 0
		}
		
		// cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
		// fetched and cached config would be considered expired because it would have been fetched
		// more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
		// throttling is in progress. The default expiration duration is 43200 (12 hours).
		remoteConfig.fetch(withExpirationDuration: expirationDuration) { (status, error) in
			if (status == .success) {
				print("Config fetched!")
				self.remoteConfig.activateFetched()
			} else {
				print("Config not fetched")
				print("Error \(String(describing: error?.localizedDescription))")
			}
		}
	}
	
}
