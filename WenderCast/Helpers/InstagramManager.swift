//
//  InstagramManager.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import WebKit
import Firebase

struct InstagramManager {
	static let cleintID =   "6d5ac0520e3546488a085dc3013e9a1f" // "7ff0d833ec08479f883c95ad592b3a41"   //
	static let redirectUri = "http://www.vesedia.com"  // "http://gomeetcity.com"
	
	private static let accessTokenKey = "InstagramManager.accessTokenKey"
	static var accessToken: String? {
		get { return UserDefaults.standard.value(forKey: InstagramManager.accessTokenKey) as? String }
		set {
			if let val = newValue {
				UserDefaults.standard.set(val, forKey: InstagramManager.accessTokenKey)
			} else {
				UserDefaults.standard.removeObject(forKey: InstagramManager.accessTokenKey)
			}
			UserDefaults.standard.synchronize()
		}
	}
	
	static var loginUrl: URL {
		
//		return URL(string: "https://instagram.com/accounts/logout")!
		
		var components = URLComponents()
		
		components.scheme = "https"
		components.host = "api.instagram.com"
		components.path = "/oauth/authorize/"
		components.queryItems = [
			URLQueryItem(name: "client_id", value: InstagramManager.cleintID),
			URLQueryItem(name: "redirect_uri", value: InstagramManager.redirectUri),
			URLQueryItem(name: "response_type", value: "token"),
			URLQueryItem(name: "scope", value: "public_content"),
			URLQueryItem(name: "DEBUG", value: "True")
		]

		guard let url = components.url else {
			fatalError()
		}
		return url
	}
	
	static func checkRequestForCallbackURL(request: URLRequest, completion:(_ shouldContinue: Bool) -> Void) {
		
		let requestURLString = (request.url?.absoluteString)! as String
		
//		print(String(describing: type(of: self)),":", #function)
		print(" - ", requestURLString)
		
		if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
			let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
			InstagramManager.handleAuth(authToken: requestURLString.substring(from: range.upperBound))
			
			if let user = AppDelegate.shared.generalUser {
				user.instagramToken = requestURLString.substring(from: range.upperBound)
				
				print(String(describing: type(of: self)),":", #function)
				print(user.instagramToken ?? "NaN")
				
				
				FirebaseStack.users.child(user.uid).child(UserFields.instagramToken).setValue(user.instagramToken)
			}
			
			completion(false)
			
		}
		completion(true)
	}
	
	static func handleAuth(authToken: String)  {
		
		print("Instagram authentication token ==", authToken)
		
		//      if let currentUser = UsersManager.currentUser {
		//         currentUser.instagramtoken = authToken
		//         UsersManager.updateUser(currentUser) { _ in
		//         }
		//      }
	}
}

