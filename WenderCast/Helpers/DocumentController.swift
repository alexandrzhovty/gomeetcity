//
//  DocumentController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/3/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
class DocumentController: UIViewController {
    //    MARK: Public
   var url: URL!
    
    //    MARK: Outlets
   @IBOutlet weak var webView: UIWebView!
    
}

//    MARK: - View life cycle
extension DocumentController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEVELOPMENT
            navigationItem.title = NSLocalizedString("Development", comment: "").localizedUppercase
        #endif
        
        
        // Customize appearance
        Appearance.customize(viewController: self)
        setupView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if webView.request?.url == nil {
            webView.loadRequest(URLRequest(url: url))
        }
        
    }
}

//    MARK: - Utilities
extension DocumentController  {
    fileprivate func setupView() {
      

    }
}

//    MARK: - Outlet functions
extension DocumentController  {
    @IBAction func didTapDone(_ button: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Navigation & SegueHandler protocol
extension DocumentController: SegueHandler {
    enum SegueType: String {
        case none
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {  
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
}
