//
//  PullDownAnimationController.swift
//  CustomTransitionDemo
//
//  Created by Robert Ryan on 2/13/17.
//  Copyright © 2017 Robert Ryan. All rights reserved.
//

import UIKit

class PullDownAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    enum TransitionType {
        case presenting
        case dismissing
    }
	
	lazy var dimmingView :UIView = { [unowned self] in
		let view = UIView()
		view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
		view.alpha = 0.0
		return view
	}()
	
    let transitionType: TransitionType
    
    init(transitionType: TransitionType) {
        self.transitionType = transitionType
        
        super.init()
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let inView   = transitionContext.containerView
        let toView   = transitionContext.view(forKey: .to)!
        let fromView = transitionContext.view(forKey: .from)!
        
        var frame = inView.bounds
        
        switch transitionType {
        case .presenting:
            frame.origin.y = -frame.size.height
			frame.size.height = 150
            toView.frame = frame
			
			dimmingView.frame = inView.bounds
			inView.addSubview(dimmingView)
			inView.addSubview(toView)
			
            UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                toView.frame.origin.y = 0
            }, completion: { finished in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        case .dismissing:
            toView.frame = frame
            inView.insertSubview(toView, belowSubview: fromView)
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                frame.origin.y = -frame.size.height
                fromView.frame = frame
            }, completion: { finished in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
	
//	override var frameOfPresentedViewInContainerView: CGRect {
//		guard let containerView = containerView else { return CGRect() }
//		
//		// We don't want the presented view to fill the whole container view, so inset it's frame
//		var frame = containerView.bounds;
//		//        let margin: CGFloat = 50.0
//		//        frame = frame.insetBy(dx: margin, dy: margin)
//		
//		frame.size.height = 150
//		
//		return frame
//	}
	
}

