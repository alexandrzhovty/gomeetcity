//
//  NearbyCollectionCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class NearbyCollectionCell: UICollectionViewCell {
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var distanceLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		contentView.backgroundColor = UIColor.white
	}
}
