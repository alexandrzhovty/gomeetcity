//
//  StatusView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class StatusView: UIView {
	@IBOutlet weak var statusLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		layer.borderColor = UIColor(.pink).cgColor
		layer.borderWidth = 1
		backgroundColor = UIColor.clear
		
		statusLabel.text = nil
		
	}
}
