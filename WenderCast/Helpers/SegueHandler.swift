//
//  SegueHandler.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol SegueHandler {
    associatedtype SegueType: RawRepresentable
}

extension SegueHandler where Self: UIViewController, SegueType.RawValue == String {
    
    func performSegue(_ identifier: SegueType, sender: Any?) {
        performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }
    
    func shouldPerformSegue(_ identifier: SegueType, sender: Any?) -> Bool {
        return shouldPerformSegue(withIdentifier: identifier.rawValue, sender: sender)
    }
    
    func segueType(for segue: UIStoryboardSegue) -> SegueType {
        guard let identifier = segue.identifier, let segueIdentifier = SegueType(rawValue: identifier) else {
            fatalError("Invalid segue identiifer \(segue.identifier ?? "NaN")")
        }
        return segueIdentifier
    }
    
    func segueType(for identifier: String?) -> SegueType {
        guard let str = identifier, let segueIdentifier = SegueType(rawValue: str) else {
            fatalError("Invalid segue identiifer \(identifier ?? "NaN")")
        }
        return segueIdentifier
    }
    
}
