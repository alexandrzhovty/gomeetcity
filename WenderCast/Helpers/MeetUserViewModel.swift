//
//  MeetUserViewModel.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/10/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import Firebase

protocol MeetUserViewModelProtocol {
	var title: String { get }
	var occupation: String? { get set }
	var readyToMeet: Bool { get set }
	var user: MeetUser { get }
	var uid: String { get }
	var gender: Gender { get set }
	var lookFor: LookFor { get set }
	var showMe: Bool { get set }
	var locationHidden: Bool { get set }
//	var notificationsEnabeld: Bool { get set }
	var profilePhoto: MeetPhoto? { get }
	var photos: [MeetPhoto] { get set }
	var photosRef: StorageReference { get }
	
}

struct MeetUserViewModel {
	fileprivate let _user: MeetUser
	init(with user: MeetUser) {
		_user = user
	}
}

extension MeetUserViewModel: MeetUserViewModelProtocol {
	var photosRef: StorageReference {
		return FirebaseStack.storageRef.child(_user.uid)
	}

	var photos: [MeetPhoto] {
		get { return _user.photos }
		set { _user.photos = newValue }
	}
	
	var profilePhoto: MeetPhoto? {
		return _user.photos.first
	}
	
	var locationHidden: Bool {
		get { return _user.locationHidden }
		set {
			_user.locationHidden = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.locationHidden).setValue(newValue)
			
		}
	}
	
	var showMe: Bool {
		get { return _user.showMe }
		set {
			_user.locationHidden = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.showMe).setValue(newValue)
			
		}
	}
	
	var lookFor: LookFor {
		get { return _user.lookFor }
		set {
			_user.lookFor = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.lookFor).setValue(newValue.rawValue)
		}
	}
	
	var gender: Gender {
		get { return _user.gender }
		set {
			_user.gender = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.gender).setValue(newValue.rawValue)
		}
	}
	
	var uid: String { return _user.uid }
	var user: MeetUser { return _user }
	var aboutMe: String? {
		get { return _user.aboutme }
		set {
			_user.aboutme = newValue ?? ""
			FirebaseStack.users.child(_user.uid).child(UserFields.aboutme).setValue(newValue)
		}
	}
	
	var title: String {
		var array: [String?] = [_user.name]
		if _user.birthday > 0 {
			let date1 = Date(timeIntervalSince1970: _user.birthday)
			if let age = Calendar.current.dateComponents([.year], from: date1, to: Date()).year {
				array.append("\(age)")
			}
		}
		
		return array.flatMap{ $0 }.joined(separator: ", ")
	}
	
	var readyToMeet: Bool {
		get { return _user.readytomeet }
		set {
			_user.readytomeet = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.readytomeet).setValue(newValue)
		}
	}
	var occupation: String? {
		get { return _user.occupation }
		set {
			_user.occupation = newValue
			FirebaseStack.users.child(_user.uid).child(UserFields.occupation).setValue(newValue)
		}
	}
}
