//
//  Strings.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/1/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation

struct Strings {
	private init() {}
}

extension Strings {
	static let OKString = NSLocalizedString("OK", comment: "OK button")
	static let CancelString = NSLocalizedString("Cancel", comment: "Label for Cancel button")
	static let DeleteString = NSLocalizedString("Delete", comment: "Label for Delete button")
}

// MARK: - Login controller
extension Strings {
	public static let LogInWithFacebook = NSLocalizedString("Log in with Facebook", comment: "Start controller")
	public static let WeDontPostAnything = NSLocalizedString("We don`t post anything on Facebook.\nBy signing in, you agree to our Privacy Policy", comment: "Start controller")
	public static let PrivacyPolicy = NSLocalizedString("Privacy Policy", comment: "")
	
	public static let InternalErrorTryLater = NSLocalizedString("Internal error. Please repeat later", comment: "")
	
	public static let RegistrationFailed = NSLocalizedString("Registration is failed", comment: "")
	
	public static let PleaseTryLater = NSLocalizedString("Please try it again later", comment: "")
}

// MARK: - Tour list data provider
extension Strings {
	public static let Tour0Text = NSLocalizedString("FIND NEW PEOPLE\nNEARBY WHO WANT\nTO MEET NOW", comment: "Tour")
	public static let Tour1Text = NSLocalizedString("Find Someone Nearby\nand Meet Them Today", comment: "Tour")
	public static let Tour2Text = NSLocalizedString("Send an Invite and Meet Now,\nin real time", comment: "Tour")
	public static let Tour3Text = NSLocalizedString("View Profiles and Rate Your\nMeeting Experiences", comment: "Tour")
	public static let Tour4Text = NSLocalizedString("Easily Navigate Map\nfor Your Meeting", comment: "Tour")
}


// MARK: - Browser
extension Strings {
	public static let UnableToOpenURLErrorTitle = NSLocalizedString("Browser", comment: "")
	public static let UnableToOpenURLError = NSLocalizedString("Unable to open URL", comment: "")
}

// MARK: - Additional info controller
extension Strings {
	public static let AdditionalInfo = NSLocalizedString("Additional Info", comment: "")
}


extension Strings {
	public static let stateExpired = NSLocalizedString("Expired", comment: "")
	public static let stateReviewing = NSLocalizedString("Reviewing", comment: "")
	public static let stateChecked = NSLocalizedString("Checked", comment: "")
	public static let stateClosed = NSLocalizedString("Closed", comment: "")
	public static let stateAccepted = NSLocalizedString("Accepted", comment: "")
	public static let stateRefused = NSLocalizedString("Refused", comment: "")
	public static let stateJoined = NSLocalizedString("Joined", comment: "")
}

extension Strings {
	public static let cannotLodPhotoFromFacebook = NSLocalizedString("Can't download photo from Facebook", comment: "")
}

// MARK: - Version view
extension Strings {
	public static let version = NSLocalizedString("Version %@", comment: "Version of the application")
}

// MARK: - User Profile
extension Strings {
	public static let ReadyToMeet = NSLocalizedString("Ready To Meet", comment: "Ready To Meet")
	public static let Settings = NSLocalizedString("Settings", comment: "Settings")
	public static let History = NSLocalizedString("History", comment: "History")
	public static let ShowMe = NSLocalizedString("Show me on Meetcity", comment: "Show me on Meetcity")
	public static let LocationHidden = NSLocalizedString("Hide My Exact Location on Map", comment: "Hide My Exact Location on Map")
	public static let NotofocationsEnabled = NSLocalizedString("Notifications", comment: "Notifications")
	public static let Sounds = NSLocalizedString("Sounds", comment: "Sounds")
}

// MARK: - Profile manager
extension Strings {
	public static let AboutMe = NSLocalizedString("About me", comment: "About me")
	public static let Occupation = NSLocalizedString("Occupation", comment: "Occupation")
	public static let ShowInstagramPhotos = NSLocalizedString("Show Instagram Photos", comment: "")
	public static let AddInstagramAccound = NSLocalizedString("Add Instagram Account", comment: "")
	public static let Iam = NSLocalizedString("I'm", comment: "I'm")
	public static let SelectedImageWillBeDeleted = NSLocalizedString("The selected image will be deleted!", comment: "The selected image will be deleted!")
}

extension Strings {
	public static let Gender = NSLocalizedString("Gender", comment: "")
	public static let Boy = NSLocalizedString("Guy", comment: "")
	public static let Girl = NSLocalizedString("Girl", comment: "")
	public static let Both = NSLocalizedString("Both", comment: "")
}

// MARK: - Profile manager
extension Strings {
	public static let Authorization = NSLocalizedString("Authorization", comment: "")
	public static let DoYouRealyWantToExit = NSLocalizedString("Do you realy want to exit", comment: "")
	public static let SignOut = NSLocalizedString("Sign out", comment: "")
}


// MARK: - Nearby list
extension Strings {
	public static let PeopleClosestToYou = NSLocalizedString("People closest to you", comment: "")
}

// MARK: - Nearby profile
extension Strings {
	public static let SendInviteForDate = NSLocalizedString("Send an Invite for a Date", comment: "")
	public static let ReportBlock = NSLocalizedString("Report/Block %@", comment: "Report/Block Rosaline")
}


