//
//  GalleryController.swift
//  LIQR
//
//  Created by Aleksandr Zhovtyi on 4/18/17.
//  Copyright © 2017 Lindenvalley GmbH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

enum GalleryViewType: Int {
    case embed
    case fullScreen
}

final class GalleryController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    var images: [UIImage]!
    var galleryViewModel: GalleryViewModel!
    
    //    MARK: Outlets
    @IBOutlet weak var containetView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //    MARK: Private
    var widthConstraint: NSLayoutConstraint!
    var heightConstraint: NSLayoutConstraint!
    
    var pageViewController: UIPageViewController!
    
    
    //    MARK: Enums & Structures
	
    
    //    MARK: - View life cycle
    deinit {
        unregisterObserver()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Constriaint
        containetView.translatesAutoresizingMaskIntoConstraints = false
        
        switch galleryViewModel.galleryViewType {
        case .fullScreen:
            
            
            containetView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
            containetView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
            
            widthConstraint = containetView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
            widthConstraint.isActive = true
            heightConstraint = containetView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height)
            heightConstraint.isActive = true
            containetView.frame = UIScreen.main.bounds
            containetView.layoutIfNeeded()
            
            registerObserver()
            
        case .embed:
            
            containetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            containetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            containetView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            containetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            closeButton.isHidden = true
            
            break
            
        }
        
        // Page controler
        pageControl.numberOfPages = galleryViewModel.numberOfObjects()
        
        if galleryViewModel.galleryViewType == .embed {
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            self.view.addGestureRecognizer(tap)
        }
        
    }
    
    //    MARK: Status bar
    override var prefersStatusBarHidden: Bool { return true }
    
    
    //    MARK: - Observers
    private func registerObserver() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(statusBarOrientationChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: UIDevice.current)
    }
    
    private func unregisterObserver() {
        let center = NotificationCenter.default
        center.removeObserver(self)
    }
    
    
    //    MARK: - Navigation
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        switch segueIdentifier(for: identifier) {  // SegueHandler
//        case .unwindToBarProfile: return true
//        case .showPageController: return true
//        }
//    }
	
	
    
    //    MARK: - Outlet functions
    
    @objc func handleTap(_ gestureRecognizer: UITapGestureRecognizer) {
        let vc = UIStoryboard(.gallery).instantiateViewController(GalleryController.self)
        vc.galleryViewModel = GalleryViewModel(user: self.galleryViewModel.user, galleryViewType: .fullScreen)
        present(vc, animated: true, completion: nil)        
    }
    
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        guard
            let vc = pageViewController.viewControllers?.first as? ImagePreviewController,
            let imageVC = pageController(for: pageControl.currentPage)
            
        else { return }
        
        let direction: UIPageViewControllerNavigationDirection = vc.pageIndex < pageControl.currentPage ? .forward : .reverse
        
        pageViewController.setViewControllers([imageVC], direction: direction, animated: true, completion: nil)
        
    }
    
    //    MARK: - Utilities
    private func rotate(to angle: CGFloat, animated: Bool) {
        
        if abs(angle) == 90 {
            widthConstraint.constant = UIScreen.main.bounds.height
            heightConstraint.constant = UIScreen.main.bounds.width
        } else {
            widthConstraint.constant = UIScreen.main.bounds.width
            heightConstraint.constant = UIScreen.main.bounds.height
        }
        
        UIView.animate(withDuration: 0.25) { [unowned self] in
            self.containetView.transform = CGAffineTransform(rotationAngle: (angle * CGFloat.pi/180.0))
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
    }
    
    //    MARK: Orientation
    @objc func statusBarOrientationChange(_ notification: Notification) {
        let orientation = UIDevice.current.orientation
        switch orientation {
        case .portrait:             rotate(to: 0, animated: true)
        case .portraitUpsideDown:   rotate(to: 0, animated: true)
        case .landscapeLeft:        rotate(to: 90, animated: true)
        case .landscapeRight:       rotate(to: -90, animated: true)
        default:                    break
        }
    }
}

// MARK: - Navigation & SegueHandler protocol
extension GalleryController: SegueHandler {
	enum SegueType: String {
		case unwindToNearbyProfile
		case showPageController
	}
	
//	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//		switch segueType(for: identifier) {
//		case .embedGallery:
//			let vc = segue.destination as! GalleryController
//			vc.galleryViewModel = GalleryViewModel(bar: self.bar, galleryViewType: .embed)
//		}
//	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .unwindToNearbyProfile:
			if UIDevice.current.orientation != .portrait {
				UIDevice.current.setValue(NSNumber(value: UIDeviceOrientation.portrait.rawValue), forKey: "orientation")
			}
		case .showPageController:
			guard let vc = pageController(for: 0) else { return }
			let pageViewController = segue.destination as! UIPageViewController
			pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
			pageViewController.dataSource = self
			pageViewController.delegate = self
			
			self.pageViewController = pageViewController
		}
	}
}


// MARK: -
extension GalleryController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    //    MARK: - Page view controller protocol
    //    MARK: Datasource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentVC = viewController as! ImagePreviewController
        let nextIndex = currentVC.pageIndex - 1
        return pageController(for: nextIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentVC = viewController as! ImagePreviewController
        let nextIndex = currentVC.pageIndex + 1
        return pageController(for: nextIndex)
    }
    
    func pageController(for pageIndex: Int) -> ImagePreviewController? {
        
        guard let image = galleryViewModel.object(at: pageIndex) else {
            return nil
        }
        
        let vc = UIStoryboard(.gallery).instantiateViewController(ImagePreviewController.self)  //   ImagePreviewController.instatiate(fromStoryboard: .gallery)
		vc.galleryViewType = self.galleryViewModel.galleryViewType
		vc.image = image
		
		
//        vc.galleryViewType = galleryViewModel.galleryViewType
//        vc.pageIndex = pageIndex
//        
//        if let image = ImageCacheManager.shared.image(for: request.url!) {
//            vc.image = image
//        } else {
//            NetworkActivity.start()
//            Alamofire.request(request).responseImage(completionHandler: { [weak vc] response in
//                NetworkActivity.stop()
//                if let image = response.result.value {
//                    if let url = response.request?.url {
//                        ImageCacheManager.shared.setImage(image, for: url)
//                    }
//                    vc?.image = image
//                }
//            })
//        }
        return vc
    }
    
    //    MARK: Delegate
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed == true {
            
            let array = pageViewController.viewControllers as! [ImagePreviewController]
            for vc in array {
                pageControl.currentPage = vc.pageIndex
            }
        }
        
        
    }
    
}
