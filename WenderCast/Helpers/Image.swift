//
//  Image.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

//    MARK: - Asset image types
protocol ImageAssetType: Testable  {}


extension UIImage {
    enum logo: String, ImageAssetType {
        case middle = "logo-middle"
    }
    
    enum icon: String, ImageAssetType {
        case profile = "Icon-Profile"
        case filter = "Icon-Filter"
        case marker = "icon-mapmarker"
        case blank = "Icon-Blank"
        case dot = "Icon-Dot"
        case dotSelected = "Icon-Dot-Selected"
        
    }
    
    enum backgorund: String, ImageAssetType {
        case navigationBar = "Backgorund-NavigationBar"
        case buttonFacebook = "Bkg-Btn-FB"
        case splash = "spash"
    }
    
    enum photo: String, ImageAssetType {
        case tourIphone = "tourIphone"
        case tourIphone2 = "tourIphone2"
        case tourIphone3 = "tourIphone3"
        case tourIphone4 = "tourIphone4"
    }
}

typealias Background = UIImage.backgorund
typealias Logo = UIImage.logo
typealias Icon = UIImage.icon
typealias Photo = UIImage.photo

//    MARK: - Initialization
extension UIImage {
    convenience init<T>(_ imageAssetType: T) where T: ImageAssetType, T: RawRepresentable, T.RawValue == String  {
        self.init(named: imageAssetType.rawValue)!
    }
}
