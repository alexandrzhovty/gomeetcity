//
//  NearbyMapController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
final class NearbyMapController: UIViewController {
    //    MARK: Public
	var parentController: NearbyListControllerProtocol?
    
    //    MARK: Outlets
	@IBOutlet weak var mapView: GMSMapView!
	@IBOutlet weak var peopleButton: UIButton!
    
    //    MARK: Private
	fileprivate lazy var _locationManager: CLLocationManager = {
		let locationManager = CLLocationManager()
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
		locationManager.distanceFilter = kCLDistanceFilterNone
		return locationManager
	}()
	fileprivate var _didFindMyLocation = false
	
    
    //    MARK: Enums & Structures
	
	deinit {
		unregisterObservers()
	}
	
}

//    MARK: - View life cycle
extension NearbyMapController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
		
        
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure tableView
		configure(mapView)
		configure(peopleButton)
		
		_locationManager.requestAlwaysAuthorization()
		
		registerObservers()
		
    }

}



//    MARK: - Observers
extension NearbyMapController {
	fileprivate func registerObservers() {
		mapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
	}
	
	fileprivate func unregisterObservers() {
		mapView.removeObserver(self, forKeyPath: "myLocation")
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		
		switch keyPath {
		case "myLocation"?:
			if !_didFindMyLocation {
				let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
				mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 10.0)
				mapView.settings.myLocationButton = true
				
				_didFindMyLocation = true
			}
		
			
		default:
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
}

//    MARK: - Utilities
extension NearbyMapController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case peopleButton:
			peopleButton.isHidden = true
		case mapView:
			mapView.isMyLocationEnabled = CLLocationManager.locationServicesEnabled()
			mapView.settings.compassButton = false
			mapView.settings.myLocationButton = false
			
			
//			// Style
//			let kMapStyle = try! String(contentsOf: Resources.JSON.mapStyle.url)
//			
//			do {
//				// Set the map style by passing a valid JSON string.
//				mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
//			} catch {
//				NSLog("One or more of the map styles failed to load. \(error)")
//			}
			
			// Delegation
//			mapView.delegate = self
			
        default: break
        }
		
    }
}

//    MARK: - Outlet functions
extension NearbyMapController  {
    //    MARK: Buttons
	@IBAction func didTapPeople(_ button: Any) {
		parentController?.setNearbyListHidden(false, animated: true)
	}
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyMapController: SegueHandler {
    enum SegueType: String {
        case none
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        switch segueType(for: identifier) {
        case .none: return true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
        case .none: break
        }
    }
}

//    MARK: - Location manager protocol
extension NearbyMapController: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		switch status {
		case CLAuthorizationStatus.authorizedAlways, CLAuthorizationStatus.authorizedWhenInUse:
			mapView.isMyLocationEnabled = true
			mapView.settings.myLocationButton = false
		default: break
		}
	}
}
