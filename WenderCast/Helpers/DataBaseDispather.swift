//
//  DataBaseDispather.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/7/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
https://bitbucket.org/snippets/alexandrzhovty/xnzpA
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import Foundation

//class DataBaseDispather {
//    static let `default` = DataBaseDispather()
//    private init() {}
//}
//

internal let DataBase = DataBaseDispather.sharedInstance

class DataBaseDispather: NSObject {
	
	static let DataBaseKey = "database"
	
	fileprivate var _currentDataBase: String?
	
	static let sharedInstance: DataBaseDispather = {
		let instance = DataBaseDispather()
		return instance
	}()
	
	var name: String {
		get {
			if let currentDataBase = _currentDataBase {
				return currentDataBase
			}
			if let currentDataBase = UserDefaults.standard.value(forKey: DataBaseDispather.DataBaseKey) as? String {
				self.name = currentDataBase
			} else {
				self.name = AppConstants.DefaultDataBase
			}
			return _currentDataBase!
		}
		set {
			_currentDataBase = newValue
			UserDefaults.standard.setValue(newValue, forKey: DataBaseDispather.DataBaseKey)
		}
	}
	
	func mainDatabase() {
		if self.name == DataBaseName.test {
			self.name = DataBaseName.main
			alert()
		}
	}
	
	func debugDatabase() {
		if self.name == DataBaseName.main {
			self.name = DataBaseName.test
			alert()
		}
	}
	
	fileprivate func alert() {
		Alert.default.showOk("Congrats!", message: "Current database is \(self.name == DataBaseName.main ? "MAIN" : "TEST") now\nPlease close app, remove from background and restart.")
	}
	

	
	func cleanPhotos(completion: @escaping () -> Void) {
		//      let cleanWorker = CleanManager()
		//      cleanWorker.cleanPhotos(completion: completion)
	}
	
	//   class var apiKey: String {
	//      let dic = ProcessInfo.processInfo.environment
	//      if dic["TEST"] != nil {
	//         return Firebase.TestServerKey
	//      } else {
	//         return Firebase.ServerKey
	//      }
	//   }
	
}
