//
//  NearbyProfileController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import MapKit


//    MARK: - Properties & variables
final class NearbyProfileController: UIViewController {
    //    MARK: Public
	var mockUser: MockUser!

    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var sendInviteButton: UIButton!
	@IBOutlet weak var reportBlockButton: UIButton!
    
    //    MARK: Private
	fileprivate let _distanceFormatter = MKDistanceFormatter()
	fileprivate var _instagramDataSource: [UIImage]!
	
	@IBAction func didTapSendInvite(_ button: Any) {
	}
	@IBAction func didTapReportBlock(_ button: Any) {
	}
	
}

//    MARK: - View life cycle
extension NearbyProfileController  {
    override func viewDidLoad() {
        super.viewDidLoad()
		
		navigationItem.title = mockUser.name
		
        // Customize appearance
        Appearance.customize(viewController: self)
		
		// Configure tableView
		configure(reportBlockButton)
		configure(sendInviteButton)
		configure(tableView)
		
		_instagramDataSource = [
			UIImage(named: "Instagram-1")!,
			UIImage(named: "Instagram-2")!,
			UIImage(named: "Instagram-3")!,
			UIImage(named: "Instagram-4")!,
			UIImage(named: "Instagram-5")!,
			UIImage(named: "Instagram-6")!
		]
		
    }
}

//    MARK: - Utilities
extension NearbyProfileController  {
    fileprivate func configure(_ view: UIView) {
        switch view {
		case sendInviteButton:
			sendInviteButton.setTitle(Strings.SendInviteForDate, for: .normal)
		
		case reportBlockButton:
			
			var userName: String
			if let name = mockUser.name.components(separatedBy: ",").first {
				userName = name
			} else {
				userName = ""
			}
			
			let title = String.localizedStringWithFormat(Strings.ReportBlock, userName)
			reportBlockButton.setTitle(title, for: .normal)
			
		case tableView:
			tableView.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.tableHeaderView?.backgroundColor = UIColor.background.forAdditionalInfo
			tableView.estimatedRowHeight = 120
			tableView.rowHeight = UITableViewAutomaticDimension
			
			tableView.register(InfoNearbyCell.self)
			tableView.register(MapNearbyCell.self)
			tableView.register(InstagramPhotosCell.self)
			tableView.register(InstagramAddAccountCell.self)
			
        default: break
        }
		
    }
}

//    MARK: - Outlet functions
extension NearbyProfileController  {
    //    MARK: Buttons
	@IBAction func didTapAddInstagramAccount(_ button: UIButton) {
//		Alert.default.showOk("Instagram", message: "Add instagram account")
		
		let vc = UIStoryboard(.utility).instantiateViewController(BrowserController.self)
		vc.presentationType = .instagram
		let navVC = UINavigationController(rootViewController: vc)
		present(navVC, animated: true, completion: nil)
		
		
//		let vc = UIStoryboard(.instagram).instantiateViewController(InstagramLoginViewController.self)
//		present(vc, animated: true, completion: nil)
		
		
	}
    //    MARK: Gesture handlers
}

// MARK: - Navigation & SegueHandler protocol
extension NearbyProfileController: SegueHandler {
    enum SegueType: String {
        case embedGallery
    }
    
	@IBAction func exitToNearbyProfileController(_ segue: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueType(for: segue) {
		case .embedGallery:
			let vc = segue.destination as! GalleryController
			vc.galleryViewModel = GalleryViewModel(user: self.mockUser, galleryViewType: .embed)
		}
    }
}

// MARK: - Table view protocol
extension NearbyProfileController: UITableViewDataSource, UITableViewDelegate {
	//	MARK: Datasource
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 4
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		switch indexPath.row {
		case 0:
			let cell = tableView.dequeueReusableCell(InfoNearbyCell.self, for: indexPath)
			cell.statusView.statusLabel.text = "Ready to Meet"
			cell.titleLabel.text = mockUser.name
			
			cell.distanceLabel.text = _distanceFormatter.string(fromDistance: mockUser.distanse)
			cell.ocuppationLabel.text = "Vesedia tester"
			cell.aboutMeLabel.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
			
			return cell
			
		case 1:
			let cell = tableView.dequeueReusableCell(MapNearbyCell.self, for: indexPath)
			return cell
		
		case 2:
			let cell = tableView.dequeueReusableCell(InstagramPhotosCell.self, for: indexPath)
			cell.collectionView.dataSource = self
			cell.collectionView.delegate = self
			return cell
			
		case 3:
			let cell = tableView.dequeueReusableCell(InstagramAddAccountCell.self, for: indexPath)
			cell.button.addTarget(self, action: #selector(didTapAddInstagramAccount(_:)), for: .touchUpInside)
			return cell
			
		default:
			let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
			return cell
		}
	}
	
	
	
	// MARK: Scroll view delegagte
	
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		if !decelerate {
			checkPosition(for: scrollView)
		}
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		checkPosition(for: scrollView)
	}
	
	/// Scroll to visible reportButton
	///
	/// - Parameter scrollView: where report button is placed int
	func checkPosition(for scrollView: UIScrollView)  {
		let delta = scrollView.contentSize.height - (scrollView.contentOffset.y + scrollView.bounds.height)
		if delta < reportBlockButton.bounds.height && delta > 0 {
			scrollView.scrollRectToVisible(reportBlockButton.superview!.frame, animated: true)
		}
	}
	
}

// MARK: - Collection view protocol
extension NearbyProfileController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return _instagramDataSource.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(InstagramCollectionCell.self, for: indexPath) as!  InstagramCollectionCell
		cell.imageView.image = _instagramDataSource[indexPath.row]
		return cell
	}
	
	//    MARK: - Flowlayout delegate
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let size = collectionView.bounds.size
		let width = (size.width - (4 * 24)) / 3
		return CGSize(width: width, height: width)
	}
	
}

