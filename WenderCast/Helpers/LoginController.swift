//
//  LoginController.swift
//  MeetCity
//
//  Created by Sergey Krotkih on 1/29/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

import Firebase
import FBSDKLoginKit
import SVProgressHUD
import SwiftyJSON


class LoginController: UIViewController {
	//    MARK: Properties & variables
	//    MARK: Public
	
	//    MARK: Outlets
	@IBOutlet weak var facebookButton: UIButton!
	@IBOutlet weak var privacyLabel: UILabel!
	@IBOutlet weak var facebookView: UIView!
	
	//    MARK: Private
	//   fileprivate var model: UsersPresenterModel = UsersManager
	//   fileprivate var currentUser: UserModel!
	fileprivate var currentUser: MeetUser!
	fileprivate var _flowManager: FlowlayoutManager!
	
	let fbLoginManager = FBSDKLoginManager()
	
	//    MARK: Enums & Structures
	enum LoginError: Error {
		case failed(reason: String)
		case facebookInfoIsNotAccessible
	}
	
	
	//    MARK: - Initialization and deini
	deinit {
		print(String(describing: type(of: self)),":", #function)
	}
	
	
	
	//    MARK: - View life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Customize appearance
		Appearance.customize(viewController: self)
		
		self.automaticallyAdjustsScrollViewInsets = false
		
		facebookButton.setTitle(Strings.LogInWithFacebook.localizedUppercase, for: .normal)
		privacyLabel.text = Strings.WeDontPostAnything
		
		let link = [NSForegroundColorAttributeName: UIColor.clickable.text]
		
		let attrString = NSMutableAttributedString(attributedString: privacyLabel.attributedText!)
		attrString.singleOut(Strings.PrivacyPolicy, with: link)
		privacyLabel.attributedText = attrString
		
		
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
		view.addGestureRecognizer(tapGesture)
		
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		// Fake 'hide' navigatin bar
		if let navBar = navigationController?.navigationBar {
			navBar.isTranslucent = true
			navBar.setBackgroundImage( UIImage(Icon.blank), for: .default)
			navBar.shadowImage = UIImage(Icon.blank)
			
		}
	}
}

// MARK: - Navigation & SegueHandler protocol
extension LoginController: SegueHandler {
	internal enum SegueType: String {
		case showDocument
		case showTourList
		case continueRegistration
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .showDocument:
			let vc = (segue.destination as! UINavigationController).topViewController as! DocumentController
			vc.title = Strings.PrivacyPolicy
			vc.url = Resources.PDF.privacyPolicy.url
			
		case .continueRegistration:
			
			
			print(self.currentUser.toDictionary())
			
			FirebaseStack.users.child(self.currentUser.uid).setValue(self.currentUser.toDictionary())
			
			let photos = self.currentUser.photos.map{ ["filename": $0.filename] }
//			print(photos)
			
			FirebaseStack.users.child(self.currentUser.uid).child(UserFields.photos).setValue(photos)
			
			
			
			SVProgressHUD.dismiss()
			
			let toViewController = segue.destination as! RegistrationController
			toViewController.currentUser = self.currentUser
//			toViewController.transitioningDelegate = self
			
		case .showTourList:     break
			
		}
	}
}

//    MARK: - Outlet functions
extension LoginController  {
	//    MARK: Buttons
	@IBAction func didTapFacebook(_ button: Any) {
		
		let failureWith = { [weak self] (error: Any) -> Void in
			if SVProgressHUD.isVisible() {
				SVProgressHUD.dismiss()
				SVProgressHUD.setDefaultMaskType(.none)
			}
			
			guard self != nil else { return }
			
			let str: String
			switch error {
			case is Error:
				str = (error as! Error).localizedDescription
			case is String:
				str = error as! String
			default:
				str = String(describing: error)
			}
			
			Alert.default.showOk(Strings.RegistrationFailed, message: str)
		}
		
		
		fbLoginManager.logOut()
		fbLoginManager.logIn(withReadPermissions: ["public_profile", "user_photos", "user_birthday", "user_work_history"], from: self) { (result, error) in
			if let error = error {
				failureWith(error)
				return
			}
			
			if result?.isCancelled == true {
				Alert.default.showOk(Strings.RegistrationFailed, message: Strings.PleaseTryLater)
				return
			}
			
			guard let accessToken = FBSDKAccessToken.current() else {
				SVProgressHUD.dismiss()
				Alert.default.showOk(Strings.RegistrationFailed, message:"Failed to get access token")
				return
			}
			
			DispatchQueue.main.async {
				SVProgressHUD.setDefaultMaskType(.black)
				SVProgressHUD.show()
			}
			
			DispatchQueue.global(qos: .utility).async {
//				self.uodateUserInfo(with: FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString))
				// Perform login by calling Firebase APIs
				let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
				Auth.auth().signIn(with: credential){ firebaseUser, error in
					
					guard error == nil else {
						failureWith(error!)
						return
					}
					
					
					self.currentUser = MeetUser(with: firebaseUser!)
					AppDelegate.shared.generalUser = self.currentUser
					
					self._flowManager = FlowlayoutManager(viewController: self, currentStep: .userUnregisteredAsMeetUser)
					self._flowManager.performNextStep(executed: .userUnregisteredAsMeetUser)

				}
				
				
			}
			
		}
	}
	
	func uodateUserInfo(with credential: AuthCredential) {
		
		let failureWith = { [weak self] (error: Any) -> Void in
			if SVProgressHUD.isVisible() {
				SVProgressHUD.dismiss()
				SVProgressHUD.setDefaultMaskType(.none)
			}
			
			guard self != nil else { return }
			
			let str: String
			switch error {
			case is Error:
				str = (error as! Error).localizedDescription
			case is String:
				str = error as! String
			default:
				str = String(describing: error)
			}
			
			Alert.default.showOk(Strings.RegistrationFailed, message: str)
		}

		
		// Perform login by calling Firebase APIs
		Auth.auth().signIn(with: credential){ firebaseUser, error in
			
			guard error == nil else {
				failureWith(error!)
				return
			}
			
			
			self.currentUser = MeetUser(with: firebaseUser!)
			
			FirebaseStack.users.child(self.currentUser.uid).observeSingleEvent(of: .value, with: {
				self.currentUser.update(with: $0)
				
				FacebookFactory.info(user: "me").request({ json in
					self.currentUser.name = json["first_name"].string
					self.currentUser.email = json["email"].string
					if self.currentUser.birthday == 0, let str = json["birthday"].string {
						let dateFormatter = DateFormatter()
						dateFormatter.dateFormat = "MM/dd/yyyy"
						if let bdate = dateFormatter.date(from: str) {
							self.currentUser.birthday = bdate.timeIntervalSince1970
						}
					}
					
					FirebaseStack.users.child(self.currentUser.uid).setValue(self.currentUser.toDictionary())
					
					DispatchQueue.main.async {
						self.performSegue(.continueRegistration, sender: nil)
					}
					
					
				}, failure: { error in
					failureWith(error)
				})
				
			})
		}
	}
	
	
	//    MARK: Gesture recognizers
	@objc func handleTap(_ gesture: UITapGestureRecognizer) {
		
		let occuarance = Strings.PrivacyPolicy
		let label = privacyLabel!
		
		guard
			let foundRange = label.attributedText?.string.range(of: occuarance),
			let range = label.attributedText?.string.nsRange(from: foundRange),
			let rect = label.boundingRectForCharacterRange(range: range)
			else {
				return
		}
		
		
		// Increase rect
		if rect.insetBy(dx: -10, dy: -15).contains(gesture.location(in: label)) == true {
			self.performSegue(.showDocument, sender: nil)
		}
		
	}
}

extension LoginController: FlowlayoutManagerViewController {
	func showDashboarad() {
		self.performSegue(.continueRegistration, sender: nil)
	}
	
	func continueRegistration() {
		self.performSegue(.continueRegistration, sender: nil)
	}
}

