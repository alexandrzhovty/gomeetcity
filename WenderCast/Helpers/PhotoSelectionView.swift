//
//  PhotoSelectionView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/6/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import Firebase


extension PhotoSelectionView: NibLoadableView {}

//@IBDesignable

protocol PhotoSelectionViewProtocol: class {
	var userModel: MeetUserViewModel! { get set }
}

extension PhotoSelectionView: PhotoSelectionViewProtocol {}


class PhotoSelectionView: UIView {
	
	var userModel: MeetUserViewModel! { didSet { configueCollection() } }
	
	var managerController: ProfileManagerController!
	
	// MARK: Outlets
	@IBOutlet weak var widthConstraint: NSLayoutConstraint!
	@IBOutlet weak var mainContainer: UIView!
	@IBOutlet weak var rightContainer: UIView!
	@IBOutlet weak var mainImageHolder: PhotoThumbView!
	@IBOutlet var imageHolders: [PhotoThumbView]!
	
	
	
	private var _items: [PhotoThumbView] {
		return imageHolders.sorted(by: { (v1, v2) -> Bool in
			return v1.tag < v2.tag
		})
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		let size = UIScreen.main.bounds.size
		self.frame.size = PhotoSelectionView.sizeThatFits(size)
		
		mainContainer.backgroundColor = UIColor.clear
		rightContainer.backgroundColor = UIColor.clear
		backgroundColor = UIColor.clear
		
//		addButtons()
		
		configueCollection()
		
		_items.forEach{
			$0.deleteButton.addTarget(self, action: #selector(didTapSelect(_:)), for: .touchUpInside)
		}
		
	}
	
	private var _itemWidth: CGFloat {
		let width = (self.bounds.width - (Constants.MainMargin * 4)) / 3 + 24
		return width
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		widthConstraint.constant = _itemWidth
	}
	
	static func sizeThatFits(_ size: CGSize) -> CGSize {
		return CGSize(width: size.width, height: size.width)
	}
	
	private func configueCollection() {
		
		_items.forEach{ $0.image = nil }

		guard let model = userModel else { return }
		
		let photosRef = model.photosRef
		for (idx, value) in model.photos.enumerated() {
			let imageRef = photosRef.child(value.filename)
			ImageCache.default.load(forKey: imageRef, completion: { [weak self] image in
				guard let `self` = self else { return }
				self._items[idx].image = image
			})
			
		}

	}
	
	@objc func didTapSelect(_ button: UIButton) {
		assert(self.viewController() is ProfileManagerController, "View controller isn't set")
		
		let idx = button.superview!.superview!.tag
		let photoHolder = _items[idx]
		
		if photoHolder.image == nil {
			let profileManagerController = self.viewController() as! ProfileManagerController
			let imagePicker = profileManagerController.imagePicker
			imagePicker.delegate = profileManagerController
			imagePicker.allowsEditing = false
			imagePicker.sourceType = .photoLibrary
			profileManagerController.present(imagePicker, animated: true, completion: nil)
			
		} else {
			let title = self.viewController()?.navigationItem.title
			let alertVC = UIAlertController(title: title, message: Strings.SelectedImageWillBeDeleted, preferredStyle: .alert)
			alertVC.addAction(UIAlertAction(title: Strings.CancelString, style: .cancel, handler: nil))
			alertVC.addAction(UIAlertAction(title: Strings.DeleteString, style: .destructive, handler: { _ in
				// Delete file
				let meetPhoto = self.userModel.photos[idx]
				let imageRef = self.userModel.photosRef.child(meetPhoto.filename)
				imageRef.delete(completion: { error in
					guard error == nil else {
						print("Cannot upload ", error!.localizedDescription)
						return
					}
					
					self.userModel.photos.remove(at: idx)
					let photos = self.userModel.photos.map{ ["filename": $0.filename] }
					FirebaseStack.users.child(self.userModel.uid).child(UserFields.photos).setValue(photos)
					self.configueCollection()
					
				})
			}))
			self.viewController()?.present(alertVC, animated: true, completion: nil)
			
		}
	}
	
	func add(_ image: UIImage) {
		let idx = userModel.photos.count
		let holder = _items[idx]
		holder.image = image
		
		let filename = UUID().uuidString + ".jpg"
		let photoRef = userModel.photosRef.child(filename)
		
		guard let data = UIImageJPEGRepresentation(image, 0) else {
			assertionFailure()
			return
		}
		
		
		photoRef.putData(data, metadata: nil) { [weak self] (metadata, error) in
			guard error == nil else {
				print("Cannot upload ", error!.localizedDescription)
				return
			}
			
			guard let `self` = self else { return }
			
			let imageRef = FirebaseStack.users.child(self.userModel.uid).child(UserFields.photos).child("\(idx)").child(PhotosFields.filename)
			imageRef.setValue(filename)
			
			let meetPhoto = MeetPhoto(with: idx, filename: filename)
			self.userModel.photos.append(meetPhoto)
			ImageCache.default.set(image, forKey: photoRef)
			
			self.configueCollection()
		}
	}
	
}

