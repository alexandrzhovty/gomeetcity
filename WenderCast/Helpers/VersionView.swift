//
//  VersionView.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/5/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

final class VersionView: UIView {
	@IBOutlet weak var versionLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		versionLabel.textColor = UIColor.text.title
		
		if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
			versionLabel.text = String.localizedStringWithFormat(Strings.version, version)
			
		} else {
			versionLabel.text = nil
		}
		
		#if DEVELOPMENT
		#endif

		if let attributedText = versionLabel.attributedText {
			let str = NSMutableAttributedString(attributedString: attributedText)
			
			str.append(NSAttributedString(string: " (\(Bundle.main.buildVersionNumber ?? "0"))\n", attributes: [:]))
			
			let style = NSMutableParagraphStyle()
			style.alignment = .center
			
			let attributes = [
				NSForegroundColorAttributeName: 	UIColor.red,
				NSFontAttributeName: 				FontFamily.Avenir.heavy.font(size: 12)
				] as [String: Any]
			
			str.append(NSAttributedString(string: NSLocalizedString("Development", comment: "").localizedUppercase, attributes: attributes))
			str.addAttributes([NSParagraphStyleAttributeName: 	style], range: NSRange(location: 0, length: str.length))
			
			versionLabel.attributedText = str
		}
		
	}
}
