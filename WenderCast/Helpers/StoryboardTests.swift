//
//  StoryboardTests.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 5/27/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import XCTest
@testable import MeetCity

class StoryboardTests: XCTestCase {
   func testStoryboards()  {
      for storyboard in StroyboadType.hashValues()  {
         XCTAssertNotNil(UIStoryboard(storyboard), storyboard.rawValue)
      }
      
      XCTAssert(true, "Passeed")
   }
   
   func testControllersInStoryboards() {
      let mainViewControllers = [
         UserProfileController.self
         
         //            DashboardController.self,
         //            NearbyListController.self,
         //            NearbyMapController.self
      ]
      
      check(mainViewControllers, in: UIStoryboard(.main))
      
      
      let utilityControllers = [
         BrowserController.self
      ]
      check(utilityControllers, in: UIStoryboard(.utility))
      
      let loginControllers = [
         LoginController.self,
         RegistrationController.self
      ]
      check(loginControllers, in: UIStoryboard(.login))
      
      
      //        let userControllers = [
      //            AdditionalInfoController.self
      //        ]
      //        check(userControllers, in: UIStoryboard(.user))
      
      
      //        let s = type(of: self)
      
      XCTAssert(true, "Passeed")
   }
   
   func check(_ viewControlles: [Any], in storyboard: UIStoryboard) {
      for vc in viewControlles {
         //            XCTAssertNotNil(storyboard.instantiateViewController(vc), "\(String(describing: storyboard)) doesn't contains \(String(describing: vc))")
         //
         //            storyboard.instantiateViewController(DashboardController.self)
         
         //            storyboard.instantiateViewController(<#T##identifier: T.Type##T.Type#>)
      }
   }
}
