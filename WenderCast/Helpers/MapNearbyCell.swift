//
//  MapNearbyCell.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/14/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

extension MapNearbyCell: NibLoadableView {}

final class MapNearbyCell: UITableViewCell {
	static let cellHeight: CGFloat = 200
	
	@IBOutlet weak var mapView: GMSMapView!
	
	var location: CLLocationCoordinate2D? {
		didSet {
			locate()
		}
	}
	

    override func awakeFromNib() {
        super.awakeFromNib()

		// Settings
		mapView.isMyLocationEnabled = false
		mapView.settings.compassButton = false
		mapView.settings.myLocationButton = false
		mapView.settings.scrollGestures = false
		mapView.settings.zoomGestures = false
		
		// Style
		let kMapStyle = try! String(contentsOf: Resources.JSON.mapStyle.url)
		do {
			mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
		} catch {
			print(String(describing: type(of: self)),":", #function, " One or more of the map styles failed to load. \(error)")
		}

    }

	override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
		var size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
		size.height = MapNearbyCell.cellHeight
		return size
	}
	
	private func locate() {
		if let coordinate = self.location {
			let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
			mapView.animate(to: camera)
		}
	}
	
}
