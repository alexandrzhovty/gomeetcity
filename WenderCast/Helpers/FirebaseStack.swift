//
//  DataService.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/8/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

/*
https://bitbucket.org/snippets/alexandrzhovty/xnzpA
 http://swiftandpainless.com/creating-a-smart-file-template/
 
 */

import Foundation
import Firebase


class FirebaseStack {
	
	let rootRef: DatabaseReference
	
	fileprivate var _users: DatabaseReference {
		return rootRef.child("user_items")
	}
	
    fileprivate static let `default` = FirebaseStack()
    private init() {
		rootRef = Database.database().reference(withPath: "ios")
	}
}

protocol FirebaseStackProtocol {
	static var users: DatabaseReference { get }
}

extension FirebaseStack {
	static var users: DatabaseReference { return FirebaseStack.default._users }
	
	static var storageRef: StorageReference { return Storage.storage().reference() }
}
