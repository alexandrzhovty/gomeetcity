//
//  UserProfileController.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

/*
 SegueHandler implementation should be added to the MeetCity
 https://bitbucket.org/snippets/alexandrzhovty/xn9G9
 */

//    MARK: - Properties & variables
extension UserProfileController: ProfilePresenterControllerProtocol {}

final class UserProfileController: UIViewController {
	//    MARK: Public
	var userModel: MeetUserViewModel!
	var dataSource: DataSource!
	
	//    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var userView: UserView!
	@IBOutlet weak var userImageView: UserImageView!
	
	
	//    MARK: Private
	fileprivate var _presenter: ProfilePresenter!
	fileprivate var _photoManger: PhotoManager!
	
	
	//    MARK: Initializations
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commotInt()
	}
	
	private func commotInt() {
		
		guard let user = AppDelegate.shared.generalUser else {
			fatalError()
		}
		
		userModel = MeetUserViewModel(with: user)
		dataSource = UserProfileDataSource(with: self)
		_presenter = ProfilePresenter(with: self)
	}
	
}

//    MARK: - View life cycle
extension UserProfileController  {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		#if DEVELOPMENT
			navigationItem.title = NSLocalizedString("Development", comment: "").localizedUppercase
		#endif
		
		// Customize appearance
		Appearance.customize(viewController: self)
		
		
		// Configure tableView
		_presenter.setup(tableView)
		
		configure(userView)
		configure(userImageView)
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		heightTableViewConstraint.constant = tableView.contentSize.height
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let indexPath = tableView.indexPathForSelectedRow {
			tableView.deselectRow(at: indexPath, animated: true)
		}
	}
}

//    MARK: - Utilities
extension UserProfileController  {
	fileprivate func configure(_ view: UIView) {
		switch view {
		case userView:
			userView.userModel = self.userModel
		case userImageView:
			userImageView.userModel = self.userModel
		default: break
		}
		
	}
}

//    MARK: - Outlet functions
extension UserProfileController  {
	//    MARK: Switcher
	@IBAction func switchDidChangeValue(_ switcher: UISwitch) {
		
	}
}

// MARK: - Navigation & SegueHandler protocol
extension UserProfileController: SegueHandler {
	enum SegueType: String {
		case showSettings
		case showHistory
		case showEditProfile
		case showNearbyList
	}
	
	@IBAction func exitToUserProfileController(_ segue: UIStoryboardSegue) {
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segueType(for: segue) {
		case .showHistory: break
//			let barButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
//			navigationController?.navigationItem.backBarButtonItem = barButton
		case .showNearbyList: break
//			let barButton = UIBarButtonItem(image: UIImage(Icon.profile), style: .plain, target: nil, action: nil)
//			navigationController?.navigationItem.backBarButtonItem = barButton
			
		case .showEditProfile:
			let vc = (segue.destination as! UINavigationController).topViewController as! ProfileManagerController
			vc.userModel = self.userModel
		
		case .showSettings:
			let vc = (segue.destination as! UINavigationController).topViewController as! SettingsController
			vc.userModel = self.userModel
		
			
		}
	}
}
