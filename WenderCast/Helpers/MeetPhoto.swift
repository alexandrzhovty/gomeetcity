//
//  Photo.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/12/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import Foundation
import SwiftyJSON

class MeetPhoto {
	var filename: String = ""
	var position: Int = 0
	
	init(with position: Int, filename: String = "") {
		self.position = position
		self.filename = filename
	}
	
	func toDictionary() -> [String : Any] {
		
		var json = JSON([:])
		
		json[MeetPhotoFields.filename].string = filename
		json[MeetPhotoFields.position].int = position
		
		return json.rawValue as! [String: Any]
	}
	
	var json: JSON {
		var json = JSON([:])
		
		json[MeetPhotoFields.filename].string = filename
		json[MeetPhotoFields.position].int = position
		
		return json
	}
}

struct MeetPhotoFields {
	static let filename = "filename"
	static let position = "position"
}
