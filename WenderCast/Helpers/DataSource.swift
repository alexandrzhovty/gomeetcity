
//
//  DataSource.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/4/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

protocol DataSourceProtocol {
	associatedtype Item
	func objectAt(at indexPath: IndexPath) -> Item
	var numberOfSection: Int { get }
	func numberOfItems(in section: Int) -> Int
	subscript(index: Int) -> SectionInfo { get }
}

extension DataSourceProtocol {
	var numberOfSection: Int { return 1 }
}



// MARK: - Section info
struct SectionInfo {
	var title: String?
	var numberOfObjects: Int
	
	
	
	init(with segment: DataSource.Segment) {
		title = segment._title
		numberOfObjects = segment._items.count
	}
}

// MARk: - Data source
class DataSource {
	
	
	typealias Item = ProfileViewModel
	var count: Int = 0
	
	fileprivate var _segments: [Segment]
	
	init(with segments: [Segment]) {
		self._segments = segments
	}
	
	
	init(with items: [Profile]) {
		self._segments = [Segment(title: nil, with: items)]
	}
}

extension DataSource {
	class Segment {
		fileprivate var _title: String?
		fileprivate var _items: [Item]
		
		init(title: String?, with items: [Profile]) {
			self._title = title
			self._items = items.map { ProfileViewModel(with: $0) }
		}
	}
}

//extension DataSource.Segment: SectionInfo {
//	var title: String? { return self._title }
//	var numberOfObjects: Int { return self._items.count }
//}




extension DataSource: DataSourceProtocol {
	var numberOfSections: Int { return _segments.count }
	
	subscript(index: Int) -> SectionInfo {
		return SectionInfo(with: _segments[index])
	}
	
	func numberOfItems(in section: Int) -> Int {
		return _segments[section]._items.count
	}
	
	func objectAt(at indexPath: IndexPath) -> Item {
		return _segments[indexPath.section]._items[indexPath.row]
	}
	
	
}

extension DataSource : Sequence, IteratorProtocol {
	
	func next() -> Int? {
		if count == 0 {
			return nil
		} else {
			defer { count -= 1 }
			return count
		}
	}
}
