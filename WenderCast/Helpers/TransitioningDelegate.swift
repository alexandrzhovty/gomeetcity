//
//  TransitioningDelegate.swift
//  MeetCity
//
//  Created by Aleksandr Zhovtyi on 6/13/17.
//  Copyright © 2017 Vesedia. All rights reserved.
//

import UIKit

class TransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
	
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		if presented == self {
			return CountryCodePresentation(presentedViewController: presented, presenting: presenting)
		}
		return nil
	}

//	weak var interactionController: UIPercentDrivenInteractiveTransition?
//	
//	func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//		return PullDownAnimationController(transitionType: .presenting)
//	}
//	
//	func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//		return PullDownAnimationController(transitionType: .dismissing)
//	}
//	
//	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
//		return PresentationController(presentedViewController: presented, presenting: presenting)
//	}
//	
//	func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
//		return interactionController
//	}
//	
//	func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
//		return interactionController
//	}
}
